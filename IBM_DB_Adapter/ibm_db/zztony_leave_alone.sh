#!/bin/bash

# Aaron - DO NOT TOUCH

#Create area to install gem outside of /PowerRuby
mkdir -p /ruby/gemsets/ruby-ibm_db
export GEM_HOME=/ruby/gemsets/ruby-ibm_db
export GEM_PATH=/ruby/gemsets/ruby-ibm_db:/PowerRuby/prV2R1/lib/ruby/gems/2.1.0
echo "GEM_HOME:$GEM_HOME"
echo "GEM_PATH:$GEM_PATH"

GEM_NAME="ibm_db-2.5.14"
rm $GEM_NAME.gem
rm -R $GEM_NAME.gem.build
echo 'build'
gem build IBM_DB.gemspec
echo "install $GEM_NAME.gem"
gem install ./$GEM_NAME.gem --local --force --ignore-dependencies --no-ri --no-rdoc 

echo "$GEM_HOME/gems/$GEM_NAME/* /PowerRuby/prV2R1/lib/ruby/gems/2.1.0/gems/$GEM_NAME-powerpc-aix-6/."
cp -R $GEM_HOME/gems/$GEM_NAME/* /PowerRuby/prV2R1/lib/ruby/gems/2.1.0/gems/$GEM_NAME-powerpc-aix-6/.

echo 'done'

