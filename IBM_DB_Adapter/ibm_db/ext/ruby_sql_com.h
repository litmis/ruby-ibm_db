/*
  +----------------------------------------------------------------------+
  |  Licensed Materials - Property of IBM                                |
  |                                                                      |
  | (C) Copyright IBM Corporation 2006, 2007, 2008, 2009, 2010, 2012     |
  +----------------------------------------------------------------------+
  | Authors: Sushant Koduru, Lynh Nguyen, Kanchana Padmanabhan,          |
  |          Dan Scott, Helmut Tessarek, Kellen Bombardier, Sam Ruby     |
  |          Ambrish Bhargava, Tarun Pasrija, Praveen Devarao            |
  |          Tony Cairns                                                 |
  +----------------------------------------------------------------------+

See README_IBM_i main directory ...    

Environment variables (external affect ext/extconf.rb):
 Compile time:
  IBM_DB_HOME            - LUW compile IBM_DB_HOME=/home/db2inst2/sqllib (LUW DB2)
  IBM_DB_UTF8            - LUW & IBM i compile with UTF-8 override (UTF8_ONLY)
 Run time:
  IBM_DB_ON_ERROR        - ALL ignore missing attributes older drivers
  IBM_DB_TRACE           - dump trace all CLI calls used
   =on                   - dump trace to STDOUT
   =/path/my.log         - dump trace to log
  IBM_DB_STOP            - trace active, force exit on SQLGetDiag
  IBM_DB_GIL             - map allow GIL/GVL release during operation

Compile will select the correct h file (ext/extconf.rb)
 ruby_sql_com.h -- PASE and LUW common include 
 ruby_sql_pase.h -- PASE only defines (ordinals different than LUW)
 ruby_sql_luw.h -- PASE only defines (ordinals different than LUW)

 Runtime ignore ruby_ibm_db_cli.c (IBM_DB_ON_ERROR):
  _ruby_ibm_db_set_attr_override
   _ruby_ibm_db_SQLSetEnvAttr_helper
   _ruby_ibm_db_SQLSetConnectAttr_helper
   _ruby_ibm_db_SQLSetStmtAttr_helper

  _ruby_ibm_db_get_attr_override 
   _ruby_ibm_db_SQLGetEnvAttr_helper
   _ruby_ibm_db_SQLGetConnectAttr_helper
   _ruby_ibm_db_SQLGetStmtAttr_helper

 ibm_db code defines (internal):
   PASE                    - compile traditional utf-8 PASE CLI APIs (IBM i)
   UTF8_ONLY               - NEW compile UTF-8 override for LUW (IBM_DB_UTF8)
   UNICODE_SUPPORT_VERSION - Ruby 1.9, 2.0 + (Ruby 1.8 is deprecated)
   UTF8_OVERRIDE_VERSION   - force utf-8 CLI APIs (see UTF8_ONLY)

 PASE chooses not support CLI wide interfaces (UTF-16) ...
  - PASE is using UTF8_OVERRIDE and SQLOverrideCCSID400 with CCSID 1208. 
    UTF-8 (1208) is asserted "good enough" for world wide business, 
    fitting PASE tooling.
  - PASE overrides various ibm_db _to_utf16 to implement UTF-8 (name remains utf16)
  - UTF8_OVERRIDE redefines all CLI wide APIs (see below),
    because wide interfaces are not required UTF-8 ...
      undef SQLWCHAR
      define SQLWCHAR SQLCHAR
      undef SQL_C_WCHAR
      define SQL_C_WCHAR SQL_C_CHAR

 PASE hack for GRAPHIC/VARGRAPHIC due to UTF-8 bug in IBM i DB2
  - workaround using UTF-16
    ifdef PASE
    define SQLWCHAR16 17
    else
    define SQLWCHAR16 (-8)
    endif
    define SQL_C_WCHAR16 SQLWCHAR16 
*/


#ifndef RUBY_SQL_COM_H
#define RUBY_SQL_COM_H
#include <ruby.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <sqlcli1.h> /* include top for overrided to work */

/* force UTF8_OVERRIDE_VERSION (non-wide CLI APIs) */
#if defined(PASE) || defined(UTF8_ONLY) 
#define UTF8_OVERRIDE_VERSION 1
#define UNICODE_SUPPORT_VERSION 1
#endif /* PASE or UTF8_ONLY */

/* utf-8 consistent (non-wide CLI APIs) */
#ifdef UTF8_OVERRIDE_VERSION
/* IBM i hack use WCHAR16 for issue with IBM i 7006 UTF-8<SQLCHAR not work>UTF-16 */
#ifdef PASE
#define SQLWCHAR16 17
#else
#define SQLWCHAR16 (-8)
#endif /* PASE */
#define SQL_C_WCHAR16 SQLWCHAR16 
#undef SQLWCHAR
#define SQLWCHAR SQLCHAR
#undef SQL_C_WCHAR
#define SQL_C_WCHAR SQL_C_CHAR
#endif /* UTF8_OVERRIDE_VERSION */

/* environment variables */
#define IBM_DB_UTF8 "IBM_DB_UTF8"
#define IBM_DB_ON_ERROR "IBM_DB_ON_ERROR"
#define IBM_DB_TRACE "IBM_DB_TRACE"
#define IBM_DB_TRACE_ERROR "IBM_DB_TRACE_ERROR"
#define IBM_DB_STOP "IBM_DB_STOP"
#define IBM_DB_GIL "IBM_DB_GIL"
#define IBM_DB_DBCS_ALLOC "IBM_DB_DBCS_ALLOC"

/* unused ordinal accross platforms */
#define SQL_ATTR_MISSING	-33333 
#define SQL_TYPE_MISSING2	-334
#define SQL_TYPE_MISSING3	-335
#define SQL_TYPE_MISSING4	-336
#define SQL_TYPE_MISSING5	-337

/* New attributes for IBM i explicit use (IBM i or LUW->IBM i) */
#define SQL_ATTR_DBC_LIBL 31666
#define SQL_ATTR_DBC_CURLIB 31667
#define SQL_MAX_QCMDEXC_LEN 32767

/* Choose LUW or PASE SQL defines
 * must follow SQL_ATTR_MISSING
 * for potential backlevel issues
 */
#ifdef PASE
#include "ruby_sql_pase.h"
#else 
#include "ruby_sql_luw.h"
#endif /* PASE */


#endif  /* RUBY_SQL_COM_H */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
