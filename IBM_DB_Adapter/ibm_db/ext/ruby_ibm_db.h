/*
  +----------------------------------------------------------------------+
  |  Licensed Materials - Property of IBM                                |
  |                                                                      |
  | (C) Copyright IBM Corporation 2006, 2007, 2008, 2009, 2010, 2012     |
  +----------------------------------------------------------------------+
  | Authors: Sushant Koduru, Lynh Nguyen, Kanchana Padmanabhan,          |
  |          Dan Scott, Helmut Tessarek, Kellen Bombardier, Sam Ruby     |
  |          Ambrish Bhargava, Tarun Pasrija, Praveen Devarao            |
  |          Tony Cairns                                                 |
  +----------------------------------------------------------------------+
*/

#ifndef RUBY_IBM_DB_H
#define RUBY_IBM_DB_H

#include "ruby_ibm_db_cli.h"  /* include here for overrided to work */


#ifdef _WIN32
#define RUBY_IBM_DB_API __declspec(dllexport)
#else
#define RUBY_IBM_DB_API
#endif

/* strlen(" SQLCODE=") added in */
#define DB2_MAX_ERR_MSG_LEN (SQL_MAX_MESSAGE_LENGTH + SQL_SQLSTATE_SIZE + 10)

/*Used to find the type of resource and the error type required*/
#define DB_ERRMSG       1
#define DB_ERR_STATE    2

#define DB_CONN         1
#define DB_STMT         2

#define CONN_ERROR      1
#define STMT_ERROR      2

/*Used to decide if LITERAL REPLACEMENT should be turned on or not*/
#define SET_QUOTED_LITERAL_REPLACEMENT_ON  1
#define SET_QUOTED_LITERAL_REPLACEMENT_OFF 0

/* DB2 instance environment variable */
#define DB2_VAR_INSTANCE "DB2INSTANCE="

/******** Makes code compatible with the options used by the user */
#define BINARY 1
#define CONVERT 2
#define PASSTHRU 3
#define PARAM_FILE 11

/*fetch*/
#define FETCH_INDEX 0x01
#define FETCH_ASSOC 0x02
#define FETCH_BOTH 0x03

/* Change column case */
#define ATTR_CASE 3271982
#define CASE_NATURAL 0
#define CASE_LOWER 1
#define CASE_UPPER 2

/* maximum sizes */
#define USERID_LEN 16
#define ACCTSTR_LEN 200
#define APPLNAME_LEN 32
#define WRKSTNNAME_LEN 18

#ifndef ROUND_HALF_EVEN /* IBM i already defined V7+ sqlcli.h */
/*
 * Enum for Decfloat Rounding Modes
 * */
enum
{
        ROUND_HALF_EVEN = 0,
        ROUND_HALF_UP,
        ROUND_DOWN,
        ROUND_CEILING,
        ROUND_FLOOR
}ROUNDING_MODE;
#endif

void Init_ibm_db();

/* Function Declarations */

VALUE ibm_db_connect(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_createDB(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_dropDB(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_createDBNX(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_commit(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_pconnect(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_autocommit(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_bind_param(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_close(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_columnprivileges(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_column_privileges(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_columns(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_foreignkeys(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_foreign_keys(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_primarykeys(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_primary_keys(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_procedure_columns(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_procedures(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_specialcolumns(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_special_columns(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_statistics(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_tableprivileges(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_table_privileges(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_tables(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_commit(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_exec(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_prepare(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_execute(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_conn_errormsg(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_stmt_errormsg(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_getErrormsg(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_getErrorstate(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_conn_error(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_stmt_error(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_next_result(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_num_fields(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_num_rows(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_result_cols(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_name(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_display_size(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_num(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_precision(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_scale(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_type(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_field_width(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_cursor_type(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_rollback(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_free_stmt(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_result(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_fetch_row(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_fetch_assoc(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_fetch_array(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_fetch_both(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_result_all(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_free_result(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_set_option(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_setoption(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_get_option(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_get_last_serial_value(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_getoption(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_fetch_object(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_server_info(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_client_info(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_active(int argc, VALUE *argv, VALUE self);

VALUE ibm_db_trace(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_trace_include(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_trace_exclude(int argc, VALUE *argv, VALUE self);
VALUE ibm_db_trace_clear(int argc, VALUE *argv, VALUE self);

/*
  Declare any global variables you may need between the BEGIN
  and END macros here:
*/
struct _ibm_db_globals {
  int  bin_mode;
#ifdef UNICODE_SUPPORT_VERSION
  SQLWCHAR  __ruby_conn_err_msg[DB2_MAX_ERR_MSG_LEN];
  SQLWCHAR  __ruby_stmt_err_msg[DB2_MAX_ERR_MSG_LEN];
  SQLWCHAR  __ruby_conn_err_state[SQL_SQLSTATE_SIZE + 1];
  SQLWCHAR  __ruby_stmt_err_state[SQL_SQLSTATE_SIZE + 1];
#else
  char      __ruby_conn_err_msg[DB2_MAX_ERR_MSG_LEN];
  char      __ruby_stmt_err_msg[DB2_MAX_ERR_MSG_LEN];
  char      __ruby_conn_err_state[SQL_SQLSTATE_SIZE + 1];
  char      __ruby_stmt_err_state[SQL_SQLSTATE_SIZE + 1];
#endif
};

/*
  TODO: make this threadsafe
*/

#define IBM_DB_G(v) (ibm_db_globals->v)

#endif  /* RUBY_IBM_DB_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */
