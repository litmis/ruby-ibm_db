/*
  +----------------------------------------------------------------------+
  |  Licensed Materials - Property of IBM                                |
  |                                                                      |
  | (C) Copyright IBM Corporation 2009, 2010, 2012                       |
  +----------------------------------------------------------------------+
  | Authors: Praveen Devarao                                             |
  |          Tony Cairns                                                 |
  +----------------------------------------------------------------------+
*/

/* ==============================
 * Story of sql_luw.h and sql_pase.h ... 
 *   see sql_com.h to understand.
 * ============================== 
 */

/*
    This C file contains functions that perform DB operations, which can take long time to complete.
    For Eg: - Like SQLConnect, SQLFetch etc.

    This file in general will contain functions that make CLI calls and 
    depending on whether the call will be transferred to server or not the functions are termed long time comsuming or not.

    The functions which will contact the server and hence can be time consuming will be called by ruby's (1.9 onwards)
    rb_thread_blocking_region method, which inturn will release the GVL while these operations are being performed. 
    With this the executing thread will become unblocking allowing concurrent threads perform operations simultaneously.
*/

#include "ruby_ibm_db.h" /* define override (see ruby_ibm_db_cli.h) */

static int _ruby_IBM_DB_ON_ERROR = -1;
static void _ruby_ibm_db_check_env_vars();
static int _ruby_ibm_db_set_attr_override(set_handle_attr_args *data);
static int _ruby_ibm_db_get_attr_override(get_handle_attr_args *data);
static int _ruby_IBM_DB_DBCS_ALLOC = -1;


static int _ruby_IBM_DB_TRACE = -1;
static int _ruby_IBM_DB_TRACE_ERROR = -1;
#define IBM_DB_TRACE_ERROR_BUFFER_MAX 4096
static char _ruby_IBM_DB_TRACE_ERROR_BUFFER[IBM_DB_TRACE_ERROR_BUFFER_MAX + 1];
static int _ruby_IBM_DB_STOP = -1;
static char * _ruby_IBM_DB_FILE = NULL;
static int _ruby_IBM_DB_GIL = -1;
static void _ruby_ibm_db_console(const char * format, ...);
static void _ruby_ibm_db_trace_hexdump(void *string, unsigned int len);
static void _ruby_ibm_db_trace_addr(void * addr);
static void _ruby_ibm_db_trace_bin_hex(int data);
static void _ruby_ibm_db_trace_sql_type(int column_type);
static void _ruby_ibm_db_trace_verbose(char * errhead, void *string, unsigned int len, int rc);
static void _ruby_ibm_db_trace_output(char * errhead, void *string, unsigned int len);
static void _ruby_ibm_db_trace_memory(char * bigbuff, int flag);


typedef struct _param_cache_node2 {
  stmt_handle *stmt_res;
  param_node *curr;
} param_node2;


/*
Connection level release GIL/GVL (see ruby_sql_com.h)
*/
int _ruby_ibm_db_SQLConnect_GIL_release = 1;          /*  0 */
int _ruby_ibm_db_SQLDisconnect_GIL_release = 1;       /*  1 */
int _ruby_ibm_db_SQLEndTran_GIL_release = 1;          /*  2 */
int _ruby_ibm_db_SQLDescribeParam_GIL_release = 1;    /*  3 */
int _ruby_ibm_db_SQLDescribeCol_GIL_release = 1;      /*  4 */
int _ruby_ibm_db_SQLBindCol_GIL_release = 1;          /*  5 */
int _ruby_ibm_db_SQLColumnPrivileges_GIL_release = 1; /*  6 */
int _ruby_ibm_db_SQLColumns_GIL_release = 1;          /*  7 */
int _ruby_ibm_db_SQLPrimaryKeys_GIL_release = 1;      /*  8 */
int _ruby_ibm_db_SQLForeignKeys_GIL_release = 1;      /*  9 */
int _ruby_ibm_db_SQLProcedureColumns_GIL_release = 1; /* 10 */
int _ruby_ibm_db_SQLProcedures_GIL_release = 1;       /* 11 */
int _ruby_ibm_db_SQLSpecialColumns_GIL_release = 1;   /* 12 */
int _ruby_ibm_db_SQLStatistics_GIL_release = 1;       /* 13 */
int _ruby_ibm_db_SQLTablePrivileges_GIL_release = 1;  /* 14 */
int _ruby_ibm_db_SQLTables_GIL_release = 1;           /* 15 */
int _ruby_ibm_db_SQLExecDirect_GIL_release = 1;       /* 16 */
int _ruby_ibm_db_SQLCreateDB_GIL_release = 1;         /* 17 */
int _ruby_ibm_db_SQLDropDB_GIL_release = 1;           /* 18 */
int _ruby_ibm_db_SQLPrepare_GIL_release = 1;          /* 19 */
int _ruby_ibm_db_SQLGetInfo_GIL_release = 1;          /* 20 */
int _ruby_ibm_db_SQLGetDiagRec_GIL_release = 1;       /* 21 */
int _ruby_ibm_db_SQLSetStmtAttr_GIL_release = 1;      /* 22 */
int _ruby_ibm_db_SQLSetConnectAttr_GIL_release = 1;   /* 23 */
int _ruby_ibm_db_SQLSetEnvAttr_GIL_release = 1;       /* 24 */
int _ruby_ibm_db_SQLGetStmtAttr_GIL_release = 1;      /* 25 */
int _ruby_ibm_db_SQLGetConnectAttr_GIL_release = 1;   /* 26 */
int _ruby_ibm_db_SQLGetEnvAttr_GIL_release = 1;       /* 27*/
/*
Statement level release GIL/GVL  (see ruby_sql_com.h)
*/
int _ruby_ibm_db_SQLFreeStmt_GIL_release = 1;         /* 28 */
int _ruby_ibm_db_SQLExecute_GIL_release = 1;          /* 29 */
int _ruby_ibm_db_SQLParamData_GIL_release = 1;        /* 30 */
int _ruby_ibm_db_SQLColAttributes_GIL_release = 1;    /* 31 */
int _ruby_ibm_db_SQLPutData_GIL_release = 1;          /* 32 */
int _ruby_ibm_db_SQLGetData_GIL_release = 1;          /* 33 */
int _ruby_ibm_db_SQLGetLength_GIL_release = 1;        /* 34 */
int _ruby_ibm_db_SQLGetSubString_GIL_release = 1;     /* 35 */
int _ruby_ibm_db_SQLNextResult_GIL_release = 1;       /* 36 */
int _ruby_ibm_db_SQLFetchScroll_GIL_release = 1;      /* 37 */
int _ruby_ibm_db_SQLFetch_GIL_release = 1;            /* 38 */
int _ruby_ibm_db_SQLNumResultCols_GIL_release = 1;    /* 39 */
int _ruby_ibm_db_SQLNumParams_GIL_release = 1;        /* 40 */
int _ruby_ibm_db_SQLRowCount_GIL_release = 1;         /* 41 */
int _ruby_ibm_db_SQLBindFileToParam_GIL_release = 1;  /* 42 */
int _ruby_ibm_db_SQLBindParameter_GIL_release = 1;    /* 43 */

static void _ruby_ibm_db_set_GIL(int i, char a) {
  switch(i) {
  case 0:
    if (a =='0') _ruby_ibm_db_SQLConnect_GIL_release= 0;
    else _ruby_ibm_db_SQLConnect_GIL_release= 1;
    break;
  case 1:
    if (a =='0') _ruby_ibm_db_SQLDisconnect_GIL_release= 0;
    else _ruby_ibm_db_SQLDisconnect_GIL_release= 1;
    break;
  case 2:
    if (a =='0') _ruby_ibm_db_SQLEndTran_GIL_release= 0;
    else _ruby_ibm_db_SQLEndTran_GIL_release= 1;
    break;
  case 3:
    if (a =='0') _ruby_ibm_db_SQLDescribeParam_GIL_release= 0;
    else _ruby_ibm_db_SQLDescribeParam_GIL_release= 1;
    break;
  case 4:
    if (a =='0') _ruby_ibm_db_SQLDescribeCol_GIL_release= 0;
    else _ruby_ibm_db_SQLDescribeCol_GIL_release= 1;
    break;
  case 5:
    if (a =='0') _ruby_ibm_db_SQLBindCol_GIL_release= 0;
    else _ruby_ibm_db_SQLBindCol_GIL_release= 1;
    break;
  case 6:
    if (a =='0') _ruby_ibm_db_SQLColumnPrivileges_GIL_release= 0;
    else _ruby_ibm_db_SQLColumnPrivileges_GIL_release= 1;
    break;
  case 7:
    if (a =='0') _ruby_ibm_db_SQLColumns_GIL_release= 0;
    else _ruby_ibm_db_SQLColumns_GIL_release= 1;
    break;
  case 8:
    if (a =='0') _ruby_ibm_db_SQLPrimaryKeys_GIL_release= 0;
    else _ruby_ibm_db_SQLPrimaryKeys_GIL_release= 1;
    break;
  case 9:
    if (a =='0') _ruby_ibm_db_SQLForeignKeys_GIL_release= 0;
    else _ruby_ibm_db_SQLForeignKeys_GIL_release= 1;
    break;
  case 10:
    if (a =='0') _ruby_ibm_db_SQLProcedureColumns_GIL_release= 0;
    else _ruby_ibm_db_SQLProcedureColumns_GIL_release= 1;
    break;
  case 11:
    if (a =='0') _ruby_ibm_db_SQLProcedures_GIL_release= 0;
    else _ruby_ibm_db_SQLProcedures_GIL_release= 1;
    break;
  case 12:
    if (a =='0') _ruby_ibm_db_SQLSpecialColumns_GIL_release= 0;
    else _ruby_ibm_db_SQLSpecialColumns_GIL_release= 1;
    break;
  case 13:
    if (a =='0') _ruby_ibm_db_SQLStatistics_GIL_release= 0;
    else _ruby_ibm_db_SQLStatistics_GIL_release= 1;
    break;
  case 14:
    if (a =='0') _ruby_ibm_db_SQLTablePrivileges_GIL_release= 0;
    else _ruby_ibm_db_SQLTablePrivileges_GIL_release= 1;
    break;
  case 15:
    if (a =='0') _ruby_ibm_db_SQLTables_GIL_release= 0;
    else _ruby_ibm_db_SQLTables_GIL_release= 1;
    break;
  case 16:
    if (a =='0') _ruby_ibm_db_SQLExecDirect_GIL_release= 0;
    else _ruby_ibm_db_SQLExecDirect_GIL_release= 1;
    break;
  case 17:
    if (a =='0') _ruby_ibm_db_SQLCreateDB_GIL_release= 0;
    else _ruby_ibm_db_SQLCreateDB_GIL_release= 1;
    break;
  case 18:
    if (a =='0') _ruby_ibm_db_SQLDropDB_GIL_release= 0;
    else _ruby_ibm_db_SQLDropDB_GIL_release= 1;
    break;
  case 19:
    if (a =='0') _ruby_ibm_db_SQLPrepare_GIL_release= 0;
    else _ruby_ibm_db_SQLPrepare_GIL_release= 1;
    break;
  case 20:
    if (a =='0') _ruby_ibm_db_SQLGetInfo_GIL_release= 0;
    else _ruby_ibm_db_SQLGetInfo_GIL_release= 1;
    break;
  case 21:
    if (a =='0') _ruby_ibm_db_SQLGetDiagRec_GIL_release= 0;
    else _ruby_ibm_db_SQLGetDiagRec_GIL_release= 1;
    break;
  case 22:
    if (a =='0') _ruby_ibm_db_SQLSetStmtAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLSetStmtAttr_GIL_release= 1;
    break;
  case 23:
    if (a =='0') _ruby_ibm_db_SQLSetConnectAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLSetConnectAttr_GIL_release= 1;
    break;
  case 24:
    if (a =='0') _ruby_ibm_db_SQLSetEnvAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLSetEnvAttr_GIL_release= 1;
    break;
  case 25:
    if (a =='0') _ruby_ibm_db_SQLGetStmtAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLGetStmtAttr_GIL_release= 1;
    break;
  case 26:
    if (a =='0') _ruby_ibm_db_SQLGetConnectAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLGetConnectAttr_GIL_release= 1;
    break;
  case 27:
    if (a =='0') _ruby_ibm_db_SQLGetEnvAttr_GIL_release= 0;
    else _ruby_ibm_db_SQLGetEnvAttr_GIL_release= 1;
    break;
  case 28:
    if (a =='0') _ruby_ibm_db_SQLFreeStmt_GIL_release= 0;
    else _ruby_ibm_db_SQLFreeStmt_GIL_release= 1;
    break;
  case 29:
    if (a =='0') _ruby_ibm_db_SQLExecute_GIL_release= 0;
    else _ruby_ibm_db_SQLExecute_GIL_release= 1;
    break;
  case 30:
    if (a =='0') _ruby_ibm_db_SQLParamData_GIL_release= 0;
    else _ruby_ibm_db_SQLParamData_GIL_release= 1;
    break;
  case 31:
    if (a =='0') _ruby_ibm_db_SQLColAttributes_GIL_release= 0;
    else _ruby_ibm_db_SQLColAttributes_GIL_release= 1;
    break;
  case 32:
    if (a =='0') _ruby_ibm_db_SQLPutData_GIL_release= 0;
    else _ruby_ibm_db_SQLPutData_GIL_release= 1;
    break;
  case 33:
    if (a =='0') _ruby_ibm_db_SQLGetData_GIL_release= 0;
    else _ruby_ibm_db_SQLGetData_GIL_release= 1;
    break;
  case 34:
    if (a =='0') _ruby_ibm_db_SQLGetLength_GIL_release= 0;
    else _ruby_ibm_db_SQLGetLength_GIL_release= 1;
    break;
  case 35:
    if (a =='0') _ruby_ibm_db_SQLGetSubString_GIL_release= 0;
    else _ruby_ibm_db_SQLGetSubString_GIL_release= 1;
    break;
  case 36:
    if (a =='0') _ruby_ibm_db_SQLNextResult_GIL_release= 0;
    else _ruby_ibm_db_SQLNextResult_GIL_release= 1;
    break;
  case 37:
    if (a =='0') _ruby_ibm_db_SQLFetchScroll_GIL_release= 0;
    else _ruby_ibm_db_SQLFetchScroll_GIL_release= 1;
    break;
  case 38:
    if (a =='0') _ruby_ibm_db_SQLFetch_GIL_release= 0;
    else _ruby_ibm_db_SQLFetch_GIL_release= 1;
    break;
  case 39:
    if (a =='0') _ruby_ibm_db_SQLNumResultCols_GIL_release= 0;
    else _ruby_ibm_db_SQLNumResultCols_GIL_release= 1;
    break;
  case 40:
    if (a =='0') _ruby_ibm_db_SQLNumParams_GIL_release= 0;
    else _ruby_ibm_db_SQLNumParams_GIL_release= 1;
    break;
  case 41:
    if (a =='0') _ruby_ibm_db_SQLRowCount_GIL_release= 0;
    else _ruby_ibm_db_SQLRowCount_GIL_release= 1;
    break;
  case 42:
    if (a =='0') _ruby_ibm_db_SQLBindFileToParam_GIL_release= 0;
    else _ruby_ibm_db_SQLBindFileToParam_GIL_release= 1;
    break;
  case 43:
    if (a =='0') _ruby_ibm_db_SQLBindParameter_GIL_release= 0;
    else _ruby_ibm_db_SQLBindParameter_GIL_release= 1;
    break;
  default:
   break;
  }
}
static void _ruby_ibm_db_talk_GIL() {
  if (_ruby_IBM_DB_TRACE_ERROR == 1) return;
  _ruby_ibm_db_console("%s\n","-------------");
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLConnect_GIL_release = %d\n",_ruby_ibm_db_SQLConnect_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLDisconnect_GIL_release = %d\n",_ruby_ibm_db_SQLDisconnect_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLEndTran_GIL_release = %d\n",_ruby_ibm_db_SQLEndTran_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLDescribeParam_GIL_release = %d\n",_ruby_ibm_db_SQLDescribeParam_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLDescribeCol_GIL_release = %d\n",_ruby_ibm_db_SQLDescribeCol_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLBindCol_GIL_release = %d\n",_ruby_ibm_db_SQLBindCol_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLColumnPrivileges_GIL_release = %d\n",_ruby_ibm_db_SQLColumnPrivileges_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLColumns_GIL_release = %d\n",_ruby_ibm_db_SQLColumns_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLPrimaryKeys_GIL_release = %d\n",_ruby_ibm_db_SQLPrimaryKeys_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLForeignKeys_GIL_release = %d\n",_ruby_ibm_db_SQLForeignKeys_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLProcedureColumns_GIL_release = %d\n",_ruby_ibm_db_SQLProcedureColumns_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLProcedures_GIL_release = %d\n",_ruby_ibm_db_SQLProcedures_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLSpecialColumns_GIL_release = %d\n",_ruby_ibm_db_SQLSpecialColumns_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLStatistics_GIL_release = %d\n",_ruby_ibm_db_SQLStatistics_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLTablePrivileges_GIL_release = %d\n",_ruby_ibm_db_SQLTablePrivileges_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLTables_GIL_release = %d\n",_ruby_ibm_db_SQLTables_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLExecDirect_GIL_release = %d\n",_ruby_ibm_db_SQLExecDirect_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLCreateDB_GIL_release = %d\n",_ruby_ibm_db_SQLCreateDB_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLDropDB_GIL_release = %d\n",_ruby_ibm_db_SQLDropDB_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLPrepare_GIL_release = %d\n",_ruby_ibm_db_SQLPrepare_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetInfo_GIL_release = %d\n",_ruby_ibm_db_SQLGetInfo_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetDiagRec_GIL_release = %d\n",_ruby_ibm_db_SQLGetDiagRec_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLSetStmtAttr_GIL_release = %d\n",_ruby_ibm_db_SQLSetStmtAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLSetConnectAttr_GIL_release = %d\n",_ruby_ibm_db_SQLSetConnectAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLSetEnvAttr_GIL_release = %d\n",_ruby_ibm_db_SQLSetEnvAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetStmtAttr_GIL_release = %d\n",_ruby_ibm_db_SQLGetStmtAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetConnectAttr_GIL_release = %d\n",_ruby_ibm_db_SQLGetConnectAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetEnvAttr_GIL_release = %d\n",_ruby_ibm_db_SQLGetEnvAttr_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLFreeStmt_GIL_release = %d\n",_ruby_ibm_db_SQLFreeStmt_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLExecute_GIL_release = %d\n",_ruby_ibm_db_SQLExecute_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLParamData_GIL_release = %d\n",_ruby_ibm_db_SQLParamData_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLColAttributes_GIL_release = %d\n",_ruby_ibm_db_SQLColAttributes_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLPutData_GIL_release = %d\n",_ruby_ibm_db_SQLPutData_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetData_GIL_release = %d\n",_ruby_ibm_db_SQLGetData_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetLength_GIL_release = %d\n",_ruby_ibm_db_SQLGetLength_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLGetSubString_GIL_release = %d\n",_ruby_ibm_db_SQLGetSubString_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLNextResult_GIL_release = %d\n",_ruby_ibm_db_SQLNextResult_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLFetchScroll_GIL_release = %d\n",_ruby_ibm_db_SQLFetchScroll_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLFetch_GIL_release = %d\n",_ruby_ibm_db_SQLFetch_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLNumResultCols_GIL_release = %d\n",_ruby_ibm_db_SQLNumResultCols_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLNumParams_GIL_release = %d\n",_ruby_ibm_db_SQLNumParams_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLRowCount_GIL_release = %d\n",_ruby_ibm_db_SQLRowCount_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLBindFileToParam_GIL_release = %d\n",_ruby_ibm_db_SQLBindFileToParam_GIL_release);
  _ruby_ibm_db_console(" _ruby_ibm_db_SQLBindParameter_GIL_release = %d\n",_ruby_ibm_db_SQLBindParameter_GIL_release);
  _ruby_ibm_db_console("%s\n","-------------");
}
static void _ruby_ibm_db_check_env_vars() {
  char a;
  int i;
  char *trace;
  char *dbcs;
  if (_ruby_IBM_DB_TRACE == -1) {
    trace = getenv(IBM_DB_TRACE);
    if (trace) {
      _ruby_IBM_DB_TRACE = 1;
      if (strcmp(trace,"on")) {
        _ruby_IBM_DB_FILE = trace;
      }
    }
    else _ruby_IBM_DB_TRACE = 0;
  }
  if (_ruby_IBM_DB_TRACE_ERROR == -1) {
    trace = getenv(IBM_DB_TRACE_ERROR);
    if (trace) _ruby_IBM_DB_TRACE_ERROR = 1;
    else _ruby_IBM_DB_TRACE_ERROR = 0;
    memset(_ruby_IBM_DB_TRACE_ERROR_BUFFER,0,IBM_DB_TRACE_ERROR_BUFFER_MAX+1);
  }
  if (_ruby_IBM_DB_STOP == -1) {
    trace = getenv(IBM_DB_STOP);
    if (trace) _ruby_IBM_DB_STOP = 1;
    else _ruby_IBM_DB_STOP = 0;
  }
  if (_ruby_IBM_DB_ON_ERROR == -1) {
    trace = getenv(IBM_DB_ON_ERROR);
    if (trace) _ruby_IBM_DB_ON_ERROR = 1;
    else _ruby_IBM_DB_ON_ERROR = 0;
  }
  if (_ruby_IBM_DB_DBCS_ALLOC == -1) {
    dbcs = getenv(IBM_DB_DBCS_ALLOC);
    if (dbcs) _ruby_IBM_DB_DBCS_ALLOC = 1;
    else _ruby_IBM_DB_DBCS_ALLOC = 0;
  }
  if (_ruby_IBM_DB_GIL == -1) {
    trace = getenv(IBM_DB_GIL);
    if (trace) {
      _ruby_IBM_DB_GIL = 1;
      for (i=0;i<strlen(trace);i++) {
        a = trace[i];
        _ruby_ibm_db_set_GIL(i,a);
      }
    }
    else _ruby_IBM_DB_GIL = 0;
    if (_ruby_IBM_DB_TRACE) {
      _ruby_ibm_db_talk_GIL();
    }
  }
}
static int _ruby_ibm_db_set_attr_override(set_handle_attr_args *data) {
  if (_ruby_IBM_DB_ON_ERROR > 0) return SQL_SUCCESS;
  return SQL_ERROR;
}
static int _ruby_ibm_db_get_attr_override(get_handle_attr_args *data) {
  if (_ruby_IBM_DB_ON_ERROR > 0) {
    *(int*)data->valuePtr == SQL_TRUE;
    return SQL_SUCCESS;
  }
  return SQL_ERROR;
}

static int forget_blocking_region( void * helper, void *args, void *UBF, void * stmt_res ) {
  int(*go)(void *) = helper;
  return go(args);
} 

static int _ruby_i5_dbcs_alloc(int col_type, int col_size) {
  /* i5_dbcs_alloc  - ADC */
  if (_ruby_IBM_DB_DBCS_ALLOC > 0) {
    switch (col_type) {
		case SQL_CHAR:
		case SQL_VARCHAR:
		case SQL_CLOB:
		case SQL_DBCLOB:
		case SQL_UTF8_CHAR:
		case SQL_WCHAR:
		case SQL_WVARCHAR:
		case SQL_GRAPHIC:
		case SQL_VARGRAPHIC:
            return col_size * 6;
		default:
			break;
	}
  }
  return col_size;
} 

/*
   Connection level Unblock function. This function is called when a thread interruput is issued while executing a
   connection level SQL call
*/
void _ruby_ibm_db_Connection_level_UBF(void *data) {
    return;
}

/*
    Statement level thread unblock function. This fuction cancels a statement level SQL call issued when requested for,
    allowing for a safe interrupt of the thread.
*/
void _ruby_ibm_db_Statement_level_UBF(stmt_handle *stmt_res) {
  int rc = 0;
  if( stmt_res->is_executing == 1 ) {
    rc = SQLCancel( (SQLHSTMT) stmt_res->hstmt );
  }
  return;
}


#ifdef PASE
static void _ruby_ibm_db_meta_helper(metadata_args *data)
{
  data->qualifier = NULL;
  data->qualifier_len = 0;
  
  if (*data->owner == '\0') {
    data->owner = NULL;
    data->owner_len = 0;
  }
  else data->owner_len = SQL_NTS;
  
  if (*data->table_name == '\0') {
    data->table_name = NULL;
    data->table_name_len = 0;
  }
  else data->table_name_len = SQL_NTS;
  
  if (*data->column_name == '\0') {
    data->column_name = NULL;
    data->column_name_len = 0;
  }
  else data->column_name_len = SQL_NTS;
}
/*
    This function sets PASE CCSID before any DB2 actions.
*/
int _ruby_ibm_db_SQLOverrideCCSID400_helper(int newCCSID) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLOverrideCCSID400", &newCCSID, sizeof(newCCSID));

  rc = (newCCSID > 0 ? SQLOverrideCCSID400(newCCSID) : SQLOverrideCCSID400(1208));

  return rc;
}
#endif /* PASE */

/*
    This function connects to the database using either SQLConnect or SQLDriverConnect CLI API
    depending on whether it is a cataloged or an uncataloged connection.
*/
int _ruby_ibm_db_SQLConnect_helper2(connect_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLConnect", data, sizeof(connect_args));

  if (!data->database && !data->uid && !data->password) {
    rc = SQLConnect( (SQLHDBC)*(data->hdbc), (SQLCHAR *)NULL,
         (SQLSMALLINT)0, (SQLCHAR *)NULL, (SQLSMALLINT)0,
         (SQLCHAR *)NULL, (SQLSMALLINT)0);
  } else {
  	if(data->ctlg_conn == 1) {
#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
    	rc = SQLConnect( (SQLHDBC)*(data->hdbc), (SQLCHAR *)data->database,
            (SQLSMALLINT)data->database_len, (SQLCHAR *)data->uid, (SQLSMALLINT)data->uid_len,
            (SQLCHAR *)data->password, (SQLSMALLINT)data->password_len );
#else
    	rc = SQLConnectW( (SQLHDBC)*(data->hdbc), data->database,
            data->database_len, data->uid, data->uid_len,
            data->password, data->password_len );
#endif
  	} else {
#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
    	rc = SQLDriverConnect( (SQLHDBC) *(data->hdbc), (SQLHWND)NULL,
            (SQLCHAR*)data->database, SQL_NTS, NULL, 0, NULL, SQL_DRIVER_NOPROMPT );
#else
    	rc = SQLDriverConnectW( (SQLHDBC) *(data->hdbc), (SQLHWND)NULL,
            data->database, SQL_NTS, NULL, 0, NULL, SQL_DRIVER_NOPROMPT );
#endif
  	}
  }
  return rc;
}

/*
    This function issues SQLDisconnect to disconnect from the Dataserver
*/
int _ruby_ibm_db_SQLDisconnect_helper2(SQLHANDLE *hdbc) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLDisconnect", hdbc, sizeof(hdbc));

  rc = SQLDisconnect( (SQLHDBC) *hdbc ); 

  return rc; 
}


/*
    This function will commit and end the inprogress transaction by issuing a SQLCommit
*/
int _ruby_ibm_db_SQLEndTran2(end_tran_args *endtran_args) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLEndTran", endtran_args, sizeof(end_tran_args));

  rc = SQLEndTran( endtran_args->handleType, *(endtran_args->hdbc), endtran_args->completionType );

  return rc;  
}

/*
   This function call the SQLDescribeParam cli call to get the description of the parameter in the sql specified
*/
int _ruby_ibm_db_SQLDescribeParam_helper2(describeparam_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLDescribeParam", data, sizeof(describeparam_args));

  data->stmt_res->is_executing = 1;

  rc = SQLDescribeParam( (SQLHSTMT) data->stmt_res->hstmt, (SQLUSMALLINT)data->param_no, &(data->sql_data_type),
                &(data->sql_precision), &(data->sql_scale), &(data->sql_nullable) );

  _ruby_ibm_db_trace_verbose("SQLDescribeParam", data, sizeof(describeparam_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function call the SQLDescribeCol cli call to get the description of the parameter in the sql specified
*/
int _ruby_ibm_db_SQLDescribeCol_helper2(describecol_args *data) {
  int i  = data->col_no - 1;
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLDescribeCol", data, sizeof(describecol_args));

  data->stmt_res->is_executing = 1;

#if defined(UNICODE_SUPPORT_VERSION) && ! defined(UTF8_OVERRIDE_VERSION)
  rc = SQLDescribeColW( (SQLHSTMT)data->stmt_res->hstmt, (SQLSMALLINT)(data->col_no),
      data->stmt_res->column_info[i].name, data->buff_length, &(data->name_length),
      &(data->stmt_res->column_info[i].type), &(data->stmt_res->column_info[i].size),
      &(data->stmt_res->column_info[i].scale), &(data->stmt_res->column_info[i].nullable) );
#else
  rc = SQLDescribeCol( (SQLHSTMT)data->stmt_res->hstmt, (SQLSMALLINT)(data->col_no),
      data->stmt_res->column_info[i].name, data->buff_length, &(data->name_length), 
      &(data->stmt_res->column_info[i].type), &(data->stmt_res->column_info[i].size), 
      &(data->stmt_res->column_info[i].scale), &(data->stmt_res->column_info[i].nullable) );
#endif


  /* i5_dbcs_alloc  - ADC */
  data->stmt_res->column_info[i].size = _ruby_i5_dbcs_alloc(data->stmt_res->column_info[i].type, data->stmt_res->column_info[i].size);

  _ruby_ibm_db_trace_verbose("SQLDescribeCol", data, sizeof(describecol_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function call the SQLBindCol cli call to get the description of the parameter in the sql specified
*/
int _ruby_ibm_db_SQLBindCol_helper2(bind_col_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLBindCol",data, sizeof(bind_col_args));

  data->stmt_res->is_executing = 1;

  rc = SQLBindCol( (SQLHSTMT) data->stmt_res->hstmt, (SQLUSMALLINT)(data->col_num),
          data->TargetType, data->TargetValuePtr, data->buff_length,
          data->out_length );

  _ruby_ibm_db_trace_verbose("SQLBindCol", data, sizeof(describecol_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLColumnPrivileges cli call to get the list of columns and the associated privileges
*/
int _ruby_ibm_db_SQLColumnPrivileges_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLColumnPrivileges", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLColumnPrivileges( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len, 
            data->column_name, data->column_name_len );
#else
  rc = SQLColumnPrivilegesW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len,
            data->column_name, data->column_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;

}

/*
   This function calls SQLColumns cli call to get the list of columns and the associated metadata of the table
*/
int _ruby_ibm_db_SQLColumns_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLColumns", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLColumns( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len,
            data->column_name, data->column_name_len );
#else
  rc = SQLColumnsW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len,
            data->column_name, data->column_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLPrimaryKeys cli call to get the list of primay key columns and the associated metadata
*/
int _ruby_ibm_db_SQLPrimaryKeys_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLPrimaryKeys", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLPrimaryKeys( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name, data->table_name_len );
#else
  rc = SQLPrimaryKeysW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name, data->table_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLForeignKeys cli call to get the list of foreign key columns and the associated metadata
*/
int _ruby_ibm_db_SQLForeignKeys_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLForeignKeys", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLForeignKeys( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name , data->table_name_len, NULL, SQL_NTS,
                NULL, SQL_NTS, NULL, SQL_NTS );
#else
  rc = SQLForeignKeysW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name , data->table_name_len, NULL, SQL_NTS,
                NULL, SQL_NTS, NULL, SQL_NTS );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLProcedureColumns cli call to get the list of parameters 
   and associated metadata of the stored procedure
*/
int _ruby_ibm_db_SQLProcedureColumns_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLProcedureColumns", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLProcedureColumns( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->proc_name, data->proc_name_len, data->column_name, data->column_name_len );
#else
  rc = SQLProcedureColumnsW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->proc_name, data->proc_name_len, data->column_name, data->column_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLProcedures cli call to get the list of stored procedures 
   and associated metadata of the stored procedure
*/
int _ruby_ibm_db_SQLProcedures_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLProcedures", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLProcedures( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->proc_name, data->proc_name_len );
#else
  rc = SQLProceduresW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->proc_name, data->proc_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLSpecialColumns cli call to get the metadata related to the special columns 
   (Unique index or primary key column) and associated metadata
*/
int _ruby_ibm_db_SQLSpecialColumns_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLSpecialColumns", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLSpecialColumns( (SQLHSTMT) data->stmt_res->hstmt, SQL_BEST_ROWID, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name, data->table_name_len,
                (SQLUSMALLINT)data->scope, SQL_NULLABLE );
#else
  rc = SQLSpecialColumnsW( (SQLHSTMT) data->stmt_res->hstmt, SQL_BEST_ROWID, data->qualifier, data->qualifier_len,
                data->owner, data->owner_len, data->table_name, data->table_name_len,
                (SQLUSMALLINT)data->scope, SQL_NULLABLE );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLStatistics cli call to get the index information for a given table. 
*/
int _ruby_ibm_db_SQLStatistics_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLStatistics", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLStatistics( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->table_name, data->table_name_len, (SQLUSMALLINT)data->unique, SQL_QUICK );
#else
  rc = SQLStatisticsW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->table_name, data->table_name_len, (SQLUSMALLINT)data->unique, SQL_QUICK );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLTablePrivileges cli call to retrieve list of tables and 
   the asscociated privileges for each table. 
*/
int _ruby_ibm_db_SQLTablePrivileges_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLTablePrivileges", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLTablePrivileges( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len );
#else
  rc = SQLTablePrivilegesW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len,
            data->owner, data->owner_len, data->table_name, data->table_name_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLTables cli call to retrieve list of tables in the specified schema and 
   the asscociated metadata for each table. 
*/
int _ruby_ibm_db_SQLTables_helper2(metadata_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLTables", data, sizeof(metadata_args));

#ifdef PASE
  _ruby_ibm_db_meta_helper(data);
#endif /* PASE */

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLTables( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->table_name, data->table_name_len, data->table_type, data->table_type_len );
#else
  rc = SQLTablesW( (SQLHSTMT) data->stmt_res->hstmt, data->qualifier, data->qualifier_len, data->owner,
          data->owner_len, data->table_name, data->table_name_len, data->table_type, data->table_type_len );
#endif

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLExecDirect cli call to execute a statement directly
*/
int _ruby_ibm_db_SQLExecDirect_helper2(exec_cum_prepare_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLExecDirect", data, sizeof(exec_cum_prepare_args));

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLExecDirect( (SQLHSTMT) data->stmt_res->hstmt, data->stmt_string, (SQLINTEGER)data->stmt_string_len );
#else
  rc = SQLExecDirectW( (SQLHSTMT) data->stmt_res->hstmt, data->stmt_string, (SQLINTEGER)data->stmt_string_len );
#endif

  _ruby_ibm_db_trace_verbose("SQLExecDirect", data, sizeof(get_data_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLCreateDb cli call
*/
int _ruby_ibm_db_SQLCreateDB_helper2(create_drop_db_args *data) {
  int rc = 0;
#ifndef PASE
#ifndef UNICODE_SUPPORT_VERSION
  #ifdef _WIN32
    HINSTANCE cliLib = NULL;
    FARPROC sqlcreatedb;
    cliLib = DLOPEN( LIBDB2 );
    sqlcreatedb =  DLSYM( cliLib, "SQLCreateDb" );
  #elif _AIX
    void *cliLib = NULL;
    typedef int (*sqlcreatedbType)( SQLHDBC, SQLCHAR *, SQLINTEGER, SQLCHAR *, SQLINTEGER, SQLCHAR *, SQLINTEGER );
    sqlcreatedbType sqlcreatedb;
  /* On AIX CLI library is in archive. Hence we will need to specify flags in DLOPEN to load a member of the archive*/
    cliLib = DLOPEN( LIBDB2, RTLD_MEMBER | RTLD_LAZY );
    sqlcreatedb = (sqlcreatedbType) DLSYM( cliLib, "SQLCreateDb" );
  #else
    void *cliLib = NULL;
    typedef int (*sqlcreatedbType)( SQLHDBC, SQLCHAR *, SQLINTEGER, SQLCHAR *, SQLINTEGER, SQLCHAR *, SQLINTEGER );
    sqlcreatedbType sqlcreatedb;
    cliLib = DLOPEN( LIBDB2, RTLD_LAZY );
    sqlcreatedb = (sqlcreatedbType) DLSYM( cliLib, "SQLCreateDb" );
  #endif
#else
  #ifdef _WIN32
    HINSTANCE cliLib = NULL;
    FARPROC sqlcreatedb;
    cliLib = DLOPEN( LIBDB2 );
    sqlcreatedb =  DLSYM( cliLib, "SQLCreateDbW" );
  #elif _AIX
    void *cliLib = NULL;
    typedef int (*sqlcreatedbType)( SQLHDBC, SQLWCHAR *, SQLINTEGER, SQLWCHAR *, SQLINTEGER, SQLWCHAR *, SQLINTEGER );
    sqlcreatedbType sqlcreatedb;
  /* On AIX CLI library is in archive. Hence we will need to specify flags in DLOPEN to load a member of the archive*/
    cliLib = DLOPEN( LIBDB2, RTLD_MEMBER | RTLD_LAZY );
    sqlcreatedb = (sqlcreatedbType) DLSYM( cliLib, "SQLCreateDbW" );
  #else
    void *cliLib = NULL;
    typedef int (*sqlcreatedbType)( SQLHDBC, SQLWCHAR *, SQLINTEGER, SQLWCHAR *, SQLINTEGER, SQLWCHAR *, SQLINTEGER );
    sqlcreatedbType sqlcreatedb;
    cliLib = DLOPEN( LIBDB2, RTLD_LAZY );
    sqlcreatedb = (sqlcreatedbType) DLSYM( cliLib, "SQLCreateDbW" );
  #endif
#endif

  rc = (*sqlcreatedb)( (SQLHSTMT) data->conn_res->hdbc, data->dbName, (SQLINTEGER)data->dbName_string_len, 
                            data->codeSet, (SQLINTEGER)data->codeSet_string_len,
							data->mode, (SQLINTEGER)data->mode_string_len );
  DLCLOSE( cliLib );
#endif /* PASE */
  return rc;
}

/*
   This function calls SQLDropDb cli call
*/
int _ruby_ibm_db_SQLDropDB_helper2(create_drop_db_args *data) {
  int rc = 0;
#ifndef PASE
#ifndef UNICODE_SUPPORT_VERSION
  #ifdef _WIN32
    HINSTANCE cliLib = NULL;
    FARPROC sqldropdb;
    cliLib = DLOPEN( LIBDB2 );
    sqldropdb =  DLSYM( cliLib, "SQLDropDb" );
  #elif _AIX
    void *cliLib = NULL;
    typedef int (*sqldropdbType)( SQLHDBC, SQLCHAR *, SQLINTEGER);
    sqldropdbType sqldropdb;
  /* On AIX CLI library is in archive. Hence we will need to specify flags in DLOPEN to load a member of the archive*/
    cliLib = DLOPEN( LIBDB2, RTLD_MEMBER | RTLD_LAZY );
    sqldropdb = (sqldropdbType) DLSYM( cliLib, "SQLDropDb" );
  #else
    void *cliLib = NULL;
    typedef int (*sqldropdbType)( SQLHDBC, SQLCHAR *, SQLINTEGER);
    sqldropdbType sqldropdb;
    cliLib = DLOPEN( LIBDB2, RTLD_LAZY );
    sqldropdb = (sqldropdbType) DLSYM( cliLib, "SQLDropDb" );
  #endif
#else
  #ifdef _WIN32
    HINSTANCE cliLib = NULL;
    FARPROC sqldropdb;
    cliLib = DLOPEN( LIBDB2 );
    sqldropdb =  DLSYM( cliLib, "SQLDropDbW" );
  #elif _AIX
    void *cliLib = NULL;
    typedef int (*sqldropdbType)( SQLHDBC, SQLWCHAR *, SQLINTEGER);
    sqldropdbType sqldropdb;
  /* On AIX CLI library is in archive. Hence we will need to specify flags in DLOPEN to load a member of the archive*/
    cliLib = DLOPEN( LIBDB2, RTLD_MEMBER | RTLD_LAZY );
    sqldropdb = (sqldropdbType) DLSYM( cliLib, "SQLDropDbW" );
  #else
    void *cliLib = NULL;
    typedef int (*sqldropdbType)( SQLHDBC, SQLWCHAR *, SQLINTEGER);
    sqldropdbType sqldropdb;
    cliLib = DLOPEN( LIBDB2, RTLD_LAZY );
    sqldropdb = (sqldropdbType) DLSYM( cliLib, "SQLDropDbW" );
  #endif
#endif

  rc = (*sqldropdb)( (SQLHSTMT) data->conn_res->hdbc, data->dbName, (SQLINTEGER)data->dbName_string_len );

  DLCLOSE( cliLib );
#endif /* PASE */
  return rc;
}

/*
   This function calls SQLPrepare cli call to prepare the given statement
*/
int _ruby_ibm_db_SQLPrepare_helper2(exec_cum_prepare_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLPrepare", data, sizeof(exec_cum_prepare_args));

  data->stmt_res->is_executing = 1;

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLPrepare( (SQLHSTMT) data->stmt_res->hstmt, data->stmt_string, (SQLINTEGER)data->stmt_string_len );
#else
  rc = SQLPrepareW( (SQLHSTMT) data->stmt_res->hstmt, data->stmt_string, (SQLINTEGER)data->stmt_string_len );
#endif

  _ruby_ibm_db_trace_verbose("SQLPrepare", data, sizeof(get_data_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLFreeStmt to end processing on the statement referenced by statement handle
*/
int _ruby_ibm_db_SQLFreeStmt_helper2(free_stmt_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLFreeStmt", data, sizeof(free_stmt_args));

  data->stmt_res->is_executing = 1;

  SQLFreeStmt((SQLHSTMT)data->stmt_res->hstmt, data->option );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLExecute cli call to execute the prepared
*/
int _ruby_ibm_db_SQLExecute_helper2(stmt_handle *stmt_res) {
  int  rc =  0;

  _ruby_ibm_db_trace_output("SQLExecute", stmt_res, sizeof(stmt_handle));

  stmt_res->is_executing = 1;

  rc = SQLExecute( (SQLHSTMT) stmt_res->hstmt );

  stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLParamData cli call to read if there is still data to be sent
*/
int _ruby_ibm_db_SQLParamData_helper2(param_cum_put_data_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLParamData", (SQLPOINTER *) &(data->valuePtr), sizeof(char *));

  data->stmt_res->is_executing = 1;

  rc = SQLParamData( (SQLHSTMT) data->stmt_res->hstmt, (SQLPOINTER *) &(data->valuePtr) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLColAttributes cli call to get the specified attribute of the column in result set
*/
int _ruby_ibm_db_SQLColAttributes_helper2(col_attr_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLColAttributes", data, sizeof(col_attr_args));

  data->stmt_res->is_executing = 1;

  rc = SQLColAttributes( (SQLHSTMT) data->stmt_res->hstmt, data->col_num,
                     data->FieldIdentifier, NULL, 0, NULL, &(data->num_attr) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLPutData cli call to supply parameter data value
*/
int _ruby_ibm_db_SQLPutData_helper2(param_cum_put_data_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLPutData", data, sizeof(param_cum_put_data_args));

  data->stmt_res->is_executing = 1;

  rc = SQLPutData( (SQLHSTMT) data->stmt_res->hstmt, (SQLPOINTER)(((param_node*)(data->valuePtr))->svalue), 
           ((param_node*)(data->valuePtr))->ivalue );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLGetData cli call to retrieve data for a single column
*/
int _ruby_ibm_db_SQLGetData_helper2(get_data_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLGetData", data, sizeof(get_data_args));

  data->stmt_res->is_executing = 1;

  rc = SQLGetData( (SQLHSTMT) data->stmt_res->hstmt, data->col_num, data->targetType, data->buff, 
            data->buff_length, data->out_length);

  _ruby_ibm_db_trace_verbose("SQLGetData", data, sizeof(get_data_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLGetLength cli call to retrieve the length of the lob value
*/
int _ruby_ibm_db_SQLGetLength_helper2(get_length_args *data) {
  int col_num = data->col_num;
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLGetLength", data, sizeof(get_length_args));

  data->stmt_res->is_executing = 1;


  rc = SQLGetLength( (SQLHSTMT) *( data->new_hstmt ), data->stmt_res->column_info[col_num-1].loc_type,
            data->stmt_res->column_info[col_num-1].lob_loc, data->sLength,
            &(data->stmt_res->column_info[col_num-1].loc_ind) );

  data->stmt_res->is_executing = 0;

  /* i5_dbcs_alloc  - ADC */
  data->sLength = _ruby_i5_dbcs_alloc(data->stmt_res->column_info[col_num-1].loc_type, data->sLength);

  return rc;
}

/*
   This function calls SQLGetSubString cli call to retrieve portion of the lob value
*/
int _ruby_ibm_db_SQLGetSubString_helper2(get_subString_args *data) {
  int col_num = data->col_num;
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLGetSubString", data, sizeof(get_subString_args));

  data->stmt_res->is_executing = 1;

  /* FromPosition(1) + ForLength - 1 (ForLength 0 <= upsets IBMi and wrong LUW anyway) */
  if (data->forLength < 1) data->forLength = 1;

  rc = SQLGetSubString( (SQLHSTMT) *( data->new_hstmt ), data->stmt_res->column_info[col_num-1].loc_type,
            data->stmt_res->column_info[col_num-1].lob_loc, 1, data->forLength, data->targetCType,
            data->buffer, data->buff_length, data->out_length, 
            &(data->stmt_res->column_info[col_num-1].loc_ind) );

  _ruby_ibm_db_trace_verbose("SQLGetSubString", data, sizeof(get_subString_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLNextResult cli call to fetch the multiple result sets that might be returned by a stored Proc
*/
int _ruby_ibm_db_SQLNextResult_helper2(next_result_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLNextResult", data, sizeof(next_result_args));

  data->stmt_res->is_executing = 1;

  rc = SQLNextResult( (SQLHSTMT) data->stmt_res->hstmt, (SQLHSTMT) *(data->new_hstmt) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLFetchScroll cli call to fetch the specified rowset of data from result
*/
int _ruby_ibm_db_SQLFetchScroll_helper2(fetch_data_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLFetchScroll", data, sizeof(fetch_data_args));

  data->stmt_res->is_executing = 1;

  rc = SQLFetchScroll( (SQLHSTMT) data->stmt_res->hstmt, data->fetchOrientation, data->fetchOffset);

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLFetch cli call to advance the cursor to 
   the next row of the result set, and retrieves any bound columns
*/
int _ruby_ibm_db_SQLFetch_helper2(fetch_data_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLFetch", data, sizeof(fetch_data_args));

  data->stmt_res->is_executing = 1;

  rc = SQLFetch( (SQLHSTMT) data->stmt_res->hstmt );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLNumResultCols cli call to fetch the number of fields contained in a result set
*/
int _ruby_ibm_db_SQLNumResultCols_helper2(row_col_count_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLNumResultCols", data, sizeof(fetch_data_args));

  data->stmt_res->is_executing = 1;

  rc = SQLNumResultCols( (SQLHSTMT) data->stmt_res->hstmt, (SQLSMALLINT*) &(data->count) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLNumParams cli call to fetch the number of parameter markers in an SQL statement
*/
int _ruby_ibm_db_SQLNumParams_helper2(row_col_count_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLNumParams", data, sizeof(row_col_count_args));

  data->stmt_res->is_executing = 1;

  rc = SQLNumParams( (SQLHSTMT) data->stmt_res->hstmt, (SQLSMALLINT*) &(data->count) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLRowCount cli call to fetch the number of rows affected by the SQL statement
*/
int _ruby_ibm_db_SQLRowCount_helper2(sql_row_count_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLRowCount", data, sizeof(sql_row_count_args));

  data->stmt_res->is_executing = 1;

  rc = SQLRowCount( (SQLHSTMT) data->stmt_res->hstmt, (SQLLEN*) &(data->count) );

  data->stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLGetInfo cli call to get general information about DBMS, which the app is connected to
*/
int _ruby_ibm_db_SQLGetInfo_helper2(get_info_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLGetInfo", data, sizeof(get_info_args));

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLGetInfo( data->conn_res->hdbc, data->infoType, data->infoValue, data->buff_length, data->out_length);
#else
  rc = SQLGetInfoW( data->conn_res->hdbc, data->infoType, data->infoValue, data->buff_length, data->out_length);
#endif

  return rc;
}

/*
   This function calls SQLGetDiagRec cli call to get the current values of a diagnostic record that contains error
*/
int _ruby_ibm_db_SQLGetDiagRec_helper2(get_diagRec_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLGetDiagRec", data, sizeof(get_diagRec_args));

#if defined(UNICODE_SUPPORT_VERSION) && ! defined(UTF8_OVERRIDE_VERSION)
  rc = SQLGetDiagRecW( data->hType, data->handle, data->recNum, data->SQLState, data->NativeErrorPtr,
                              data->msgText, data->buff_length, data->text_length_ptr );
#else
  rc = SQLGetDiagRec(data->hType, data->handle, data->recNum, data->SQLState, data->NativeErrorPtr,
                              data->msgText, data->buff_length, data->text_length_ptr );
#endif

  _ruby_ibm_db_trace_verbose("SQLGetDiagRec", data, sizeof(get_diagRec_args), rc);

  if (_ruby_IBM_DB_TRACE_ERROR == 1) {
    _ruby_ibm_db_trace_memory((char *)NULL,0);
  }
  
  return rc;
}

/*
   This function calls SQLSetStmtAttr cli call to set attributes related to a statement
*/
int _ruby_ibm_db_SQLSetStmtAttr_helper2(set_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLSetStmtAttr", data, sizeof(set_handle_attr_args));

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLSetStmtAttr( (SQLHSTMT) *(data->handle), data->attribute, data->valuePtr, data->strLength );
#else
  rc = SQLSetStmtAttrW( (SQLHSTMT) *(data->handle), data->attribute, data->valuePtr, data->strLength );
#endif

  if (_ruby_ibm_db_set_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLSetStmtAttr", data, sizeof(set_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLSetConnectAttr cli call to set attributes that govern aspects of connections
*/
int _ruby_ibm_db_SQLSetConnectAttr_helper2(set_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLSetConnectAttr", data, sizeof(set_handle_attr_args));

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLSetConnectAttr( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr, data->strLength );
#else
  rc = SQLSetConnectAttrW( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr, data->strLength );
#endif

  if (_ruby_ibm_db_set_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLSetConnectAttr", data, sizeof(set_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLSetEnvAttr cli call to set an environment attribute
*/
int _ruby_ibm_db_SQLSetEnvAttr_helper2(set_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLSetEnvAttr", data, sizeof(set_handle_attr_args));

  rc = SQLSetEnvAttr( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr, data->strLength );

  if (_ruby_ibm_db_set_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLSetEnvAttr", data, sizeof(set_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLGetStmtAttr cli call to set an statement attribute

   The unicode equivalent of SQLGetStmtAttr is not used because the attributes being retrieved currently are not of type char or binary (SQL_IS_INTEGER). If support for retrieving a string type is provided then use the SQLGetStmtAttrW function accordingly
   In get_last_serial_id although we are retrieving a char type, it is converted back to an integer (atoi). The char to integer conversion function in unicode equivalent will be more complicated and is unnecessary for this case.

*/
int _ruby_ibm_db_SQLGetStmtAttr_helper2(get_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLGetStmtAttr", data, sizeof(get_handle_attr_args));

  if (_ruby_ibm_db_get_attr_override(data) == SQL_SUCCESS) return SQL_SUCCESS;

  rc = SQLGetStmtAttr( (SQLHSTMT) *(data->handle), data->attribute, data->valuePtr, 
            data->buff_length, data->out_length);

  if (_ruby_ibm_db_get_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLGetStmtAttr", data, sizeof(get_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLGetConnectAttr cli call to get a connection attribute
*/
int _ruby_ibm_db_SQLGetConnectAttr_helper2(get_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLGetConnectAttr", data, sizeof(get_handle_attr_args));

#if ! defined(UNICODE_SUPPORT_VERSION) || defined(UTF8_OVERRIDE_VERSION)
  rc = SQLGetConnectAttr( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr, 
            data->buff_length, data->out_length);
#else
  rc = SQLGetConnectAttrW( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr,
            data->buff_length, data->out_length);
#endif

  if (_ruby_ibm_db_get_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLGetConnectAttr", data, sizeof(get_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLGetEnvAttr cli call to get an environment attribute
*/
int _ruby_ibm_db_SQLGetEnvAttr_helper2(get_handle_attr_args *data) {
  int rc = SQL_ERROR;

  _ruby_ibm_db_trace_output("SQLGetEnvAttr", data, sizeof(get_handle_attr_args));

  rc = SQLGetEnvAttr( (SQLHDBC) *(data->handle), data->attribute, data->valuePtr, 
            data->buff_length, data->out_length);

  if (_ruby_ibm_db_get_attr_override(data) == SQL_SUCCESS) rc = SQL_SUCCESS;

  _ruby_ibm_db_trace_verbose("SQLGetEnvAttr", data, sizeof(get_handle_attr_args), rc);

  return rc;
}

/*
   This function calls SQLBindFileToParam cli call
 
SQLRETURN SQLBindFileToParam (
             SQLHSTMT          StatementHandle,
             SQLUSMALLINT      TargetType,
             SQLSMALLINT       DataType,
             SQLCHAR           *FileName,
             SQLSMALLINT       *FileNameLength,  <--- bug LUW
             SQLUINTEGER       *FileOptions,
             SQLSMALLINT       MaxFileNameLength,
             SQLINTEGER        *IndicatorValue); 
*/
int _ruby_ibm_db_SQLBindFileToParam_helper2(param_node2 *data) {
  int rc = 0;
  stmt_handle *stmt_res = data->stmt_res;
  param_node *curr = data->curr;
  SQLSMALLINT FileNameLength = curr->ivalue;

  _ruby_ibm_db_trace_output("SQLBindFileToParam", curr, sizeof(param_node));

  stmt_res->is_executing = 1;

  /* FIX LUW (@ADC 7/23/2013): 
   * ERROR ivalue truncation -- (SQLSMALLINT*)&(curr->ivalue) 
   * typical cast above -- &(FileNameLength)
   */
  rc = SQLBindFileToParam( (SQLHSTMT)stmt_res->hstmt, curr->param_num,
           curr->data_type, (SQLCHAR*)curr->svalue,
           &(FileNameLength), &(curr->file_options), 
           curr->ivalue, &(curr->bind_indicator) );

  stmt_res->is_executing = 0;

  return rc;
}

/*
   This function calls SQLBindParameter cli call
*/
int _ruby_ibm_db_SQLBindParameter_helper2(bind_parameter_args *data) {
  int rc = 0;

  _ruby_ibm_db_trace_output("SQLBindParameter", data, sizeof(bind_parameter_args));

  data->stmt_res->is_executing = 1;

  rc = SQLBindParameter( (SQLHSTMT) data->stmt_res->hstmt, data->param_num, data->IOType, data->valueType,
           data->paramType, data->colSize, data->decimalDigits, data->paramValPtr, data->buff_length,
           data->out_length );

  _ruby_ibm_db_trace_verbose("SQLBindParameter", data, sizeof(bind_parameter_args), rc);

  data->stmt_res->is_executing = 0;

  return rc;
}
/*
Connection level release GIL/GVL
*/
int _ruby_ibm_db_SQLConnect_helper(connect_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLConnect_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLConnect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLConnect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLDisconnect_helper(SQLHANDLE *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLDisconnect_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLDisconnect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLDisconnect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLEndTran(end_tran_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLEndTran_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLEndTran2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLEndTran2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLDescribeParam_helper(describeparam_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLDescribeParam_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLDescribeParam_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLDescribeParam_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLDescribeCol_helper(describecol_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLDescribeCol_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLDescribeCol_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLDescribeCol_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLBindCol_helper(bind_col_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLBindCol_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLBindCol_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLBindCol_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLColumnPrivileges_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLColumnPrivileges_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLColumnPrivileges_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLColumnPrivileges_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLColumns_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLColumns_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLPrimaryKeys_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLPrimaryKeys_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLPrimaryKeys_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLPrimaryKeys_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLForeignKeys_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLForeignKeys_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLForeignKeys_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLForeignKeys_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLProcedureColumns_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLProcedureColumns_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLProcedureColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLProcedureColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLProcedures_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLProcedures_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLProcedures_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLProcedures_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLSpecialColumns_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLSpecialColumns_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLSpecialColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLSpecialColumns_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLStatistics_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLStatistics_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLStatistics_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLStatistics_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLTablePrivileges_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLTablePrivileges_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLTablePrivileges_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLTablePrivileges_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLTables_helper(metadata_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLTables_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLTables_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLTables_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLExecDirect_helper(exec_cum_prepare_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLExecDirect_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLExecDirect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLExecDirect_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLCreateDB_helper(create_drop_db_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLCreateDB_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLCreateDB_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLCreateDB_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLDropDB_helper(create_drop_db_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLDropDB_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLDropDB_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLDropDB_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLPrepare_helper(exec_cum_prepare_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLPrepare_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLPrepare_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLPrepare_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLGetInfo_helper(get_info_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetInfo_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetInfo_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetInfo_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLGetDiagRec_helper(get_diagRec_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetDiagRec_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetDiagRec_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetDiagRec_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLSetStmtAttr_helper(set_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLSetStmtAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLSetStmtAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLSetStmtAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLSetConnectAttr_helper(set_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLSetConnectAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLSetConnectAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLSetConnectAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLSetEnvAttr_helper(set_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLSetEnvAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLSetEnvAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLSetEnvAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLGetStmtAttr_helper(get_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetStmtAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetStmtAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetStmtAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLGetConnectAttr_helper(get_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetConnectAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetConnectAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetConnectAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
int _ruby_ibm_db_SQLGetEnvAttr_helper(get_handle_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetEnvAttr_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetEnvAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetEnvAttr_helper2, data,
                         (void *)_ruby_ibm_db_Connection_level_UBF, NULL);
  return rc;
}
/*
Statement level release GIL/GVL
*/
int _ruby_ibm_db_SQLFreeStmt_helper(free_stmt_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLFreeStmt_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLFreeStmt_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLFreeStmt_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLExecute_helper(stmt_handle *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLExecute_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLExecute_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLExecute_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data);
  return rc;
}
int _ruby_ibm_db_SQLParamData_helper(param_cum_put_data_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLParamData_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLParamData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLParamData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLColAttributes_helper(col_attr_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLColAttributes_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLColAttributes_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLColAttributes_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLPutData_helper(param_cum_put_data_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLPutData_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLPutData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLPutData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLGetData_helper(get_data_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetData_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetData_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLGetLength_helper(get_length_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetLength_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetLength_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetLength_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLGetSubString_helper(get_subString_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLGetSubString_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLGetSubString_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLGetSubString_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLNextResult_helper(next_result_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLNextResult_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLNextResult_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLNextResult_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLFetchScroll_helper(fetch_data_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLFetchScroll_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLFetchScroll_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLFetchScroll_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLFetch_helper(fetch_data_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLFetch_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLFetch_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLFetch_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLNumResultCols_helper(row_col_count_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLNumResultCols_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLNumResultCols_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLNumResultCols_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLNumParams_helper(row_col_count_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLNumParams_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLNumParams_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLNumParams_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLRowCount_helper(sql_row_count_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLRowCount_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLRowCount_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLRowCount_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLBindFileToParam_helper(stmt_handle *stmt_res, param_node *curr) {
  int rc = 0;
  param_node2 indata;
  param_node2 *data = (param_node2 *)&indata;
  indata.stmt_res = stmt_res; 
  indata.curr = curr;
  if (_ruby_ibm_db_SQLBindFileToParam_GIL_release) 
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLBindFileToParam_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLBindFileToParam_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}
int _ruby_ibm_db_SQLBindParameter_helper(bind_parameter_args *data) {
  int rc = 0;
  if (_ruby_ibm_db_SQLBindParameter_GIL_release)
  rc = rb_thread_blocking_region( (void *)_ruby_ibm_db_SQLBindParameter_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  else
  rc = forget_blocking_region( (void *)_ruby_ibm_db_SQLBindParameter_helper2, data,
                         (void *)_ruby_ibm_db_Statement_level_UBF, data->stmt_res);
  return rc;
}


/*
    ==========================================================
    Handy CLI trace dump functions 
*/
static void _ruby_ibm_db_console(const char * format, ...) {
    char bigbuff[4096];
    char *p = (char *) &bigbuff; 
    FILE * pFile;

    va_list args;
    va_start(args, format);
    vsprintf(p, format, args);
    va_end(args);

    if (_ruby_IBM_DB_TRACE_ERROR == 1) {
      _ruby_ibm_db_trace_memory(bigbuff,1);
      return;
    }
    if (! _ruby_IBM_DB_FILE) {
      printf("%s",p);
    }
    else {
      pFile = fopen (_ruby_IBM_DB_FILE,"a+");
      if (pFile!=NULL)
      {
        fputs (p,pFile);
        fclose (pFile);
      }
    }
}
static void _ruby_ibm_db_trace_memory(char * bigbuff, int flag) {
  int have, want, remain, need;
  char tmp[IBM_DB_TRACE_ERROR_BUFFER_MAX+1];
  /* dump data -- start over */
  if (!flag) {
    _ruby_IBM_DB_TRACE_ERROR = 0;
    _ruby_ibm_db_console("%s\n", _ruby_IBM_DB_TRACE_ERROR_BUFFER);
    memset(_ruby_IBM_DB_TRACE_ERROR_BUFFER,0,IBM_DB_TRACE_ERROR_BUFFER_MAX+1);
    _ruby_IBM_DB_TRACE_ERROR = 1;
    return;
  }
  /* save data -- last in */
  want = strlen(bigbuff);
  have = strlen(_ruby_IBM_DB_TRACE_ERROR_BUFFER);
  remain = IBM_DB_TRACE_ERROR_BUFFER_MAX-have;
  if (remain < want) {
    need = want - remain;
    memset(tmp,0,IBM_DB_TRACE_ERROR_BUFFER_MAX);
    strcpy(tmp,&_ruby_IBM_DB_TRACE_ERROR_BUFFER[need]);
    memset(_ruby_IBM_DB_TRACE_ERROR_BUFFER,0,IBM_DB_TRACE_ERROR_BUFFER_MAX+1);
    strcpy(_ruby_IBM_DB_TRACE_ERROR_BUFFER,tmp);
  }
  strcat(_ruby_IBM_DB_TRACE_ERROR_BUFFER,bigbuff);
  _ruby_IBM_DB_TRACE_ERROR_BUFFER[IBM_DB_TRACE_ERROR_BUFFER_MAX] = '\0';
}
static void _ruby_ibm_db_trace_hexdump(void *string, unsigned int len) {
  char	charbuf[17];
  char	c0, c1;
  char	*p = (char*)string;
  char	*end;
  unsigned int i;

  /* dump less */
  if (_ruby_IBM_DB_TRACE_ERROR == 1) {
    if (len > 64) {
      len = 64;
    }
  } else {
    if (len > 512) {
      len = 512;
    }
  }

  end = p + len;
  for (i = 0; p < end ; ++p, ++i)
  {
	if ((i & 3) == 0)
	    _ruby_ibm_db_console(" ");
	if ((i & 7) == 0)
	    _ruby_ibm_db_console(" ");
	if ((i & 15) == 0)
	{
	    if (i > 0)
	    {
		charbuf[16] = 0;
		_ruby_ibm_db_console(">%s<", charbuf);
	    }
	    _ruby_ibm_db_console("\n         %08x : ", p);
	}

	charbuf[i & 15] = (isgraph(*p)? *p: '.');
	c0 = '0' + ((unsigned char)*p >> 4);
	if (c0 > '9')
	    c0 = c0 - '9' + 'a' - 1;

	c1 = '0' + (*p & 0x0f);
	if (c1 > '9')
	    c1 = c1 - '9' + 'a' - 1;

	_ruby_ibm_db_console("%c%c", c0, c1);
    }

    for (; (i & 15) != 0; ++i)
    {
	if ((i & 3) == 0)
	    _ruby_ibm_db_console(" ");
	if ((i & 7) == 0)
	    _ruby_ibm_db_console(" ");
	charbuf[i & 15] = 0;
	_ruby_ibm_db_console("  ");
  }

  charbuf[16] = 0;
  _ruby_ibm_db_console("  >%s<\n", charbuf);
}
static void _ruby_ibm_db_trace_addr(void * addr) {
  _ruby_ibm_db_console(" %x\n",addr);
}
static void _ruby_ibm_db_trace_bin_hex(int data) {
  _ruby_ibm_db_console(" %d (%x)\n",data,data);
}
static void _ruby_ibm_db_trace_sql_type(int column_type) {
  _ruby_ibm_db_console(" ");
  switch(column_type) {
    case SQL_CHAR:
      _ruby_ibm_db_console("SQL_CHAR");
      break;
    case SQL_WCHAR:
      _ruby_ibm_db_console("SQL_WCHAR");
      break;
    case SQL_VARCHAR:
      _ruby_ibm_db_console("SQL_VARCHAR");
      break;
    case SQL_WVARCHAR:
      _ruby_ibm_db_console("SQL_WVARCHAR");
      break;
    case SQL_GRAPHIC:
      _ruby_ibm_db_console("SQL_GRAPHIC");
      break;
    case SQL_VARGRAPHIC:
      _ruby_ibm_db_console("SQL_VARGRAPHIC");
      break;
#ifndef PASE /* IBM i SQL_LONGVARCHAR is SQL_VARCHAR */
    case SQL_LONGVARCHAR:
      _ruby_ibm_db_console("SQL_LONGVARCHAR");
      break;
    case SQL_WLONGVARCHAR:
      _ruby_ibm_db_console("SQL_WLONGVARCHAR");
      break;
#endif /* PASE */
    case SQL_BINARY:
      _ruby_ibm_db_console("SQL_BINARY");
      break;
#ifndef PASE /* IBM i SQL_LONGVARBINARY is SQL_VARBINARY */
    case SQL_LONGVARBINARY:
      _ruby_ibm_db_console("SQL_LONGVARBINARY");
      break;
#endif /* PASE */
    case SQL_VARBINARY:
      _ruby_ibm_db_console("SQL_VARBINARY");
      break;
    case SQL_TYPE_DATE:
      _ruby_ibm_db_console("SQL_TYPE_DATE");
      break;
    case SQL_TYPE_TIME:
      _ruby_ibm_db_console("SQL_TYPE_TIME");
      break;
    case SQL_TYPE_TIMESTAMP:
      _ruby_ibm_db_console("SQL_TYPE_TIMESTAMP");
      break;
    case SQL_BIGINT:
      _ruby_ibm_db_console("SQL_BIGINT");
      break;
    case SQL_DECFLOAT:
      _ruby_ibm_db_console("SQL_DECFLOAT");
      break;
    case SQL_SMALLINT:
      _ruby_ibm_db_console("SQL_SMALLINT");
      break;
    case SQL_INTEGER:
      _ruby_ibm_db_console("SQL_INTEGER");
      break;
    case SQL_REAL:
      _ruby_ibm_db_console("SQL_REAL");
      break;
    case SQL_FLOAT:
      _ruby_ibm_db_console("SQL_FLOAT");
      break;
    case SQL_DOUBLE:
      _ruby_ibm_db_console("SQL_DOUBLE");
      break;
    case SQL_DECIMAL:
      _ruby_ibm_db_console("SQL_DECIMAL");
      break;
    case SQL_NUMERIC:
      _ruby_ibm_db_console("SQL_NUMERIC");
      break;
    case SQL_CLOB:
      _ruby_ibm_db_console("SQL_CLOB");
      break;
    case SQL_DBCLOB:
      _ruby_ibm_db_console("SQL_DBCLOB");
      break;
    case SQL_BLOB:
      _ruby_ibm_db_console("SQL_BLOB");
      break;
    case SQL_XML:
      _ruby_ibm_db_console("SQL_XML");
      break;
    case SQL_BLOB_LOCATOR:
      _ruby_ibm_db_console("SQL_BLOB_LOCATOR");
      break;
    case SQL_CLOB_LOCATOR:
      _ruby_ibm_db_console("SQL_CLOB_LOCATOR");
      break;
    case SQL_DBCLOB_LOCATOR:
      _ruby_ibm_db_console("SQL_DBCLOB_LOCATOR");
      break;
    case SQL_C_DEFAULT:
      _ruby_ibm_db_console("SQL_C_DEFAULT");
      break;
    default:
      _ruby_ibm_db_console("UNKNOWN");
      break;
  }
  _ruby_ibm_db_trace_bin_hex(column_type);
}
static void _ruby_ibm_db_trace_verbose(char * errhead, void *string, unsigned int len, int rc) {
  exec_cum_prepare_args *prepexec = (exec_cum_prepare_args *) string;
  get_diagRec_args *diagrec = (get_diagRec_args *) string;
  get_data_args *getdata = (get_data_args *) string;
  get_subString_args *subdata = (get_subString_args *) string;
  describecol_args *describecol = (describecol_args *) string;
  bind_col_args *bindcol = (bind_col_args *) string;
  bind_parameter_args *bindparm = (bind_parameter_args *) string;
  describeparam_args *describeparm = (describeparam_args *) string;
  set_handle_attr_args *setattr = (set_handle_attr_args*) string;
  get_handle_attr_args *getattr = (get_handle_attr_args *) string;
  int max = 128;
  int i = 0;

  _ruby_ibm_db_check_env_vars();

  if (_ruby_IBM_DB_TRACE < 1) return;
  
  if (!string || len < 1) return;
  
  _ruby_ibm_db_console("     %s = %d\n",errhead,rc);

  
  /* special output */
  if (!strcmp(errhead,"SQLBindParameter")) {
    _ruby_ibm_db_console("     bindparm nbr =");
    _ruby_ibm_db_trace_bin_hex(bindparm->param_num);
    _ruby_ibm_db_console("     bindparm iotype =");
    _ruby_ibm_db_trace_sql_type(bindparm->IOType);
    _ruby_ibm_db_console("     bindparm valuetype =");
    _ruby_ibm_db_trace_sql_type(bindparm->valueType);
    _ruby_ibm_db_console("     bindparm paramtype =");
    _ruby_ibm_db_trace_sql_type(bindparm->paramType);
    _ruby_ibm_db_console("     bindparm colsize =");
    _ruby_ibm_db_trace_bin_hex(bindparm->colSize);
    _ruby_ibm_db_console("     bindparm scale =");
    _ruby_ibm_db_trace_bin_hex(bindparm->decimalDigits);
    _ruby_ibm_db_console("     bindparm buff =");
    _ruby_ibm_db_trace_addr(bindparm->paramValPtr);
    _ruby_ibm_db_console("     bindparm buff_len =");
    _ruby_ibm_db_trace_bin_hex(bindparm->buff_length);
    _ruby_ibm_db_console("     bindparm out_len =");
    _ruby_ibm_db_trace_addr(bindparm->out_length);
  }
  else if (!strcmp(errhead,"SQLBindCol")) {
    _ruby_ibm_db_console("     bindcol nbr =");
    _ruby_ibm_db_trace_bin_hex(bindcol->col_num);
    _ruby_ibm_db_console("     bindcol type =");
    _ruby_ibm_db_trace_sql_type(bindcol->TargetType);
    _ruby_ibm_db_console("     bindcol buff =");
    _ruby_ibm_db_trace_addr(bindcol->TargetValuePtr);
    _ruby_ibm_db_console("     bindcol buff_len =");
    _ruby_ibm_db_trace_bin_hex(bindcol->buff_length);
    _ruby_ibm_db_console("     bindcol out_len =");
    _ruby_ibm_db_trace_addr(bindcol->out_length);
  }
  else if (!strcmp(errhead,"SQLDescribeParam")) {
    _ruby_ibm_db_console("     parm nbr =");
    _ruby_ibm_db_trace_bin_hex(describeparm->param_no);
    _ruby_ibm_db_console("     parm type =");
    _ruby_ibm_db_trace_sql_type(describeparm->sql_data_type);
    _ruby_ibm_db_console("     parm size =");
    _ruby_ibm_db_trace_bin_hex(describeparm->sql_precision);
    _ruby_ibm_db_console("     parm scale =");
    _ruby_ibm_db_trace_bin_hex(describeparm->sql_scale);
    _ruby_ibm_db_console("     parm nullable =");
    _ruby_ibm_db_trace_bin_hex(describeparm->sql_nullable);
  }
  else if (!strcmp(errhead,"SQLDescribeCol")) {
    i  = describecol->col_no - 1;
    if (describecol->name_length > 0) {
      if (max > describecol->name_length) {
      	max = describecol->name_length;
        _ruby_ibm_db_console("     col name");
      }
      else {
        _ruby_ibm_db_console("     col name...");
      }
      _ruby_ibm_db_trace_hexdump(describecol->stmt_res->column_info[i].name, max);
    }
    _ruby_ibm_db_console("     col nbr =");
    _ruby_ibm_db_trace_bin_hex(describecol->col_no);
    _ruby_ibm_db_console("     col type =");
    _ruby_ibm_db_trace_sql_type(describecol->stmt_res->column_info[i].type);
    _ruby_ibm_db_console("     col size =");
    _ruby_ibm_db_trace_bin_hex(describecol->stmt_res->column_info[i].size);
    _ruby_ibm_db_console("     col scale =");
    _ruby_ibm_db_trace_bin_hex(describecol->stmt_res->column_info[i].scale);
    _ruby_ibm_db_console("     col nullable =");
    _ruby_ibm_db_trace_bin_hex(describecol->stmt_res->column_info[i].nullable);
  }
  else if (!strcmp(errhead,"SQLGetData")) {
    _ruby_ibm_db_console("     nbr =");
    _ruby_ibm_db_trace_bin_hex(getdata->col_num);
    _ruby_ibm_db_console("     type =");
    _ruby_ibm_db_trace_sql_type(getdata->targetType);
    _ruby_ibm_db_console("     buf len =");
    _ruby_ibm_db_trace_bin_hex(getdata->buff_length);
    if (getdata->out_length && *getdata->out_length == SQL_NULL_DATA) {
        _ruby_ibm_db_console("     buff is SQL_NULL_DATA\n");
    }
    else if (getdata->out_length && *getdata->out_length > 0) {
      _ruby_ibm_db_console("     out len =");
      _ruby_ibm_db_trace_bin_hex(*getdata->out_length);
      if (max > *getdata->out_length) {
      	max = *getdata->out_length;
        _ruby_ibm_db_console("     buff");
      }
      else {
        _ruby_ibm_db_console("     buff...");
      }
      _ruby_ibm_db_trace_hexdump(getdata->buff, max);
    }
  }
  else if (!strcmp(errhead,"SQLGetSubString")) {
    _ruby_ibm_db_console("     nbr =");
    _ruby_ibm_db_trace_bin_hex(subdata->col_num);
    _ruby_ibm_db_console("     c type =");
    _ruby_ibm_db_trace_sql_type(subdata->targetCType);
    _ruby_ibm_db_console("     for len =");
    _ruby_ibm_db_trace_bin_hex(subdata->forLength);
    _ruby_ibm_db_console("     buf len =");
    _ruby_ibm_db_trace_bin_hex(subdata->buff_length);
    if (subdata->out_length && *subdata->out_length > 0) {
      if (max > *subdata->out_length) {
      	max = *subdata->out_length;
        _ruby_ibm_db_console("     buff");
      }
      else {
        _ruby_ibm_db_console("     buff...");
      }
      _ruby_ibm_db_trace_hexdump(subdata->buffer, max);
    }
  }
  else if (!strcmp(errhead,"SQLGetDiagRec")) {
    if (diagrec->SQLState) {
      _ruby_ibm_db_console("     state");
      _ruby_ibm_db_trace_hexdump(diagrec->SQLState, 5);
    }
    if (diagrec->NativeErrorPtr) {
      _ruby_ibm_db_console("     code");
      _ruby_ibm_db_trace_hexdump(diagrec->NativeErrorPtr, sizeof(SQLINTEGER));
    }
    if (diagrec->msgText && diagrec->text_length_ptr && *diagrec->text_length_ptr > 0) {
      if (max > *diagrec->text_length_ptr) {
      	max = *diagrec->text_length_ptr;
        _ruby_ibm_db_console("     message");
      }
      else {
        _ruby_ibm_db_console("     message...");
      }
      _ruby_ibm_db_trace_hexdump(diagrec->msgText, *diagrec->text_length_ptr);
    }
    if (_ruby_IBM_DB_STOP > 0) exit(-999);
  }
  else if (!strcmp(errhead,"SQLPrepare")) {
    if (prepexec->stmt_string && prepexec->stmt_string_len > 0) {
      _ruby_ibm_db_console("     prepare");
      _ruby_ibm_db_trace_hexdump(prepexec->stmt_string, prepexec->stmt_string_len);
    }
  }
  else if (!strcmp(errhead,"SQLExecDirect")) {
    if (prepexec->stmt_string && prepexec->stmt_string_len > 0) {
      _ruby_ibm_db_console("     exec");
      _ruby_ibm_db_trace_hexdump(prepexec->stmt_string, prepexec->stmt_string_len);
    }
  }
  else if (!strcmp(errhead,"SQLGetEnvAttr") || !strcmp(errhead,"SQLGetConnectAttr") || !strcmp(errhead,"SQLGetStmtAttr")) {
    _ruby_ibm_db_console("     attr =");
    _ruby_ibm_db_trace_bin_hex(getattr->attribute);
    if (getattr->buff_length < 1) {
#ifndef PASE
      _ruby_ibm_db_console("     value =");
      _ruby_ibm_db_trace_bin_hex((int)getattr->valuePtr);
#else /* PASE */
      _ruby_ibm_db_console("     value =");
      _ruby_ibm_db_trace_bin_hex(*(int*)getattr->valuePtr);
#endif /* PASE */
    }
    else {
      if (max > getattr->buff_length) {
        max = getattr->buff_length;
      }
      _ruby_ibm_db_console("     value");
      _ruby_ibm_db_trace_hexdump(getattr->valuePtr, max);
    }
  }
  else if (!strcmp(errhead,"SQLSetEnvAttr") || !strcmp(errhead,"SQLSetConnectAttr") || !strcmp(errhead,"SQLSetStmtAttr")) {
    _ruby_ibm_db_console("     attr =");
    _ruby_ibm_db_trace_bin_hex(setattr->attribute);
    if (setattr->strLength < 1) {
#ifndef PASE
      _ruby_ibm_db_console("     value =");
      _ruby_ibm_db_trace_bin_hex((int)setattr->valuePtr);
#else /* PASE */
      _ruby_ibm_db_console("     value =");
      _ruby_ibm_db_trace_bin_hex(*(int*)setattr->valuePtr);
#endif /* PASE */
    }
    else {
      if (max > setattr->strLength) {
        max = setattr->strLength;
      }
      _ruby_ibm_db_console("     value");
      _ruby_ibm_db_trace_hexdump(setattr->valuePtr, max);
    }
  }
}
static void _ruby_ibm_db_trace_output(char * errhead, void *string, unsigned int len) {
  _ruby_ibm_db_check_env_vars();

  if (_ruby_IBM_DB_TRACE < 1) return;

  _ruby_ibm_db_console("%s\n","-------------");
#ifdef UNICODE_SUPPORT_VERSION
#ifdef UTF8_OVERRIDE_VERSION
  _ruby_ibm_db_console("%s (utf-8 non-wide)",errhead);
#else
  _ruby_ibm_db_console("%s (utf-16 wide)",errhead);
#endif
#else
  _ruby_ibm_db_console("%s (non-wide)",errhead);
#endif
  _ruby_ibm_db_trace_hexdump(string, len);  
}

