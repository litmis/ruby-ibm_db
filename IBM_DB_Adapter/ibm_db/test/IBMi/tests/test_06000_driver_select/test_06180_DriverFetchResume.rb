# File: test_06180_DriverFetchResume.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06180_DriverFetchResume < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from EMP_RESUME')
    while row = IBM_DB::fetch_array(stmt)
      puts row[0]
      assert_match( $resumes[idx][0], row[0], "EMPNO mismatch")
      assert_match( $resumes[idx][1], row[1], "FORMAT mismatch")
      resume = File.dirname(File.realpath(__FILE__)) + "/../test_artifacts/#{$resumes[idx][2]}"
      size = File.size(resume)
      data = IO.read(resume,size)
      assert_equal(size, data.size, "read size mismatch")
      assert_equal(data.size, row[2].size, "db2 size mismatch")
      assert_equal(data, row[2], "resume mismatch")
      idx += 1
    end
  end

end

