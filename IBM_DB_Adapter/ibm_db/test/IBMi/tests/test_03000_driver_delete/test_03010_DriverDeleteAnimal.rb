# File: test_03010_DriverDeleteAnimal.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_03010_DriverDeleteAnimal < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
    opts = {IBM_DB::SQL_ATTR_AUTOCOMMIT => IBM_DB::SQL_AUTOCOMMIT_OFF}
	IBM_DB::set_option(@@conn,opts,1)
  end

  def test_0002_delete
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(id) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count start")

    # delete animals matching criteria
    stmt = IBM_DB::exec(@@conn,'DELETE FROM animals WHERE weight > 10.0')
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(3,nbr,"mismatch nbr rows affected")

    # rollback changes 
    IBM_DB::rollback(@@conn)
    stmt = IBM_DB::exec(@@conn,'select count(id) from animals WHERE weight > 10.0')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(3,row[0],"mismatch count after rollback")
  end

  def test_0003_delete
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count start")

    # delete all animals
    stmt = IBM_DB::exec(@@conn,'DELETE FROM animals')
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(7,nbr,"mismatch nbr rows affected")
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(0,row[0],"mismatch count after delete")

    # rollback to restore all
    IBM_DB::rollback(@@conn)
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")
  end


  def test_0004_commit
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")

    # autocommit on
    IBM_DB::autocommit( @@conn, IBM_DB::SQL_AUTOCOMMIT_ON );
    value = IBM_DB::autocommit( @@conn );
    assert_equal(1,value,"mismatch autocommit not on")

    # autocommit off
    IBM_DB::autocommit( @@conn, IBM_DB::SQL_AUTOCOMMIT_OFF );
    value = IBM_DB::autocommit( @@conn );
    assert_equal(0,value,"mismatch autocommit not off")

    # delete all animals
    stmt = IBM_DB::exec(@@conn,'DELETE FROM animals')
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(7,nbr,"mismatch nbr rows affected")
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(0,row[0],"mismatch count after delete")

    # commit now
    IBM_DB::commit(@@conn)
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(0,row[0],"mismatch count after commit")

    # re-insert all data
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ANIMALS (ID, BREED, NAME, WEIGHT) VALUES (?, ?, ?, ?)')
    $animals.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }

    # commit now
    IBM_DB::commit(@@conn)
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after commit")

  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
