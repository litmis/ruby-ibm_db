# File: test_01090_DriverPrepareTableAnimalPicsFile.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_01090_DriverPrepareTableAnimalPicsFile < Test::Unit::TestCase
  @@conn = nil

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end


  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE ANIMAL_PICS_FILE')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE ANIMAL_PICS_FILE (NAME VARCHAR(32), PICTURE BLOB(5M))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ANIMAL_PICS_FILE (NAME, PICTURE) VALUES (?, ?)')
    $animal_pics.each { |a| 
      name = a[0]
      picname = a[1]
      picture = File.dirname(File.realpath(__FILE__)) + "/../test_artifacts/#{picname}"
      puts picture
      ret = IBM_DB::bind_param(stmt, 1, "name", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 2, "picture", IBM_DB::PARAM_FILE)
      ret = IBM_DB::execute(stmt)
    }
  end

end
