# File: test_08310_DriverTableXT.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08310_DriverTableXT < Test::Unit::TestCase
  @@conn = nil
  @@table_order_meta = ['XT1', 'XT2', 'XT3', 'XT4']
  @@prv_order_meta = ['ALTER','DELETE', 'INDEX', 'INSERT', 'REFERENCES', 'SELECT', 'UPDATE']

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0003_meta_tables
    idx = 0
    stmt = IBM_DB::tables(@@conn,nil, $config[:username],'XT%')
    while row = IBM_DB::fetch_assoc(stmt)
      puts "#{idx}: #{row}"
      # TABLE_CAT:    The catalog that contains the table. The value is NULL if this table does not have catalogs.
      # TABLE_SCHEMA: Name of the schema that contains the table.
      # TABLE_NAME:   Name of the table.
      # TABLE_TYPE:   Table type identifier for the table.
      # REMARKS:      Description of the table. 
      assert_match( $config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
      assert_match(@@table_order_meta[idx],row['table_name'],"mismatch TABLE_NAME")
      assert_match('TABLE',row['table_type'],"mismatch TABLE_TYPE")
      idx += 1
    end
    count = @@table_order_meta.count
    assert_equal(count, idx, "missing row data")
  end

  def test_0004_meta_table_privileges
    idx = 0
    prv = 0
    stmt = IBM_DB::table_privileges(@@conn,nil, $config[:username],'XT%')
    while row = IBM_DB::fetch_assoc(stmt)
      puts "#{idx} #{row}"
      # TABLE_CAT:    The catalog that contains the table. The value is NULL if this table does not have catalogs.
      # TABLE_SCHEM:  Name of the schema that contains the table.
      # TABLE_NAME:   Name of the table.
      # GRANTOR:      Authorization ID of the user who granted the privilege.
      # GRANTEE:      Authorization ID of the user to whom the privilege was granted.
      # PRIVILEGE:    The privilege that has been granted. This can be one of ALTER, CONTROL, DELETE, INDEX, INSERT, REFERENCES, SELECT, or UPDATE.
      # IS_GRANTABLE: A string value of "YES" or "NO" indicating whether the grantee can grant the privilege to other users. 
      assert_match( $config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
      assert_match(@@table_order_meta[idx],row['table_name'],"mismatch TABLE_NAME")
      assert_nil(row['grantor'],"mismatch GRANTOR")
      assert_match( $config[:username],row['grantee'],"mismatch GRANTEE")
      assert_match(@@prv_order_meta[prv],row['privilege'],"mismatch PRIVILEGE")
      assert_match('YES',row['is_grantable'],"mismatch IS_GRANTABLE")
      prv += 1
      if prv == @@prv_order_meta.count
        prv = 0
        idx += 1
      end
    end
    count = @@table_order_meta.count
    assert_equal(count, idx, "missing row data")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
