# File: test_08410_DriverPrimaryForeignKeysTest.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08410_DriverPrimaryForeignKeysTest < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0003_meta_primary_keys
    idx = 0
    # TABLE_CAT:   Name of the catalog for the table containing the primary key. The value is NULL if this table does not have catalogs.
    # TABLE_SCHEM: Name of the schema for the table containing the primary key.
    # TABLE_NAME:  Name of the table containing the primary key.
    # COLUMN_NAME: Name of the column containing the primary key.
    # KEY_SEQ:     1-indexed position of the column in the key.
    # PK_NAME:     The name of the primary key.
    found = nil 
    stmt = IBM_DB::primary_keys(@@conn, nil,  $config[:username], 'test_primary_keys')
    while row = IBM_DB::fetch_assoc(stmt)
      found = true 
      puts "#{idx}: #{row}"
      assert_match( $config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
      assert_match('test_primary_keys',row['table_name'],"mismatch TABLE_NAME")
      assert_match('ID',row['column_name'],"mismatch COLUMN_NAME")
      assert_equal(1,row['key_seq'],"mismatch KEY_SEQ")
      assert_match("Q_#{ $config[:username]}_TEST_00001_ID_00001",row['pk_name'],"mismatch PK_NAME")
    end
    assert_not_nil(found,"mismatch no records found #{ $config[:username]}")
  end

  def test_0004_meta_foreign_keys
    idx = 0
    # PKTABLE_CAT:   Name of the catalog for the table containing the primary key. The value is NULL if this table does not have catalogs.
    # PKTABLE_SCHEM: Name of the schema for the table containing the primary key.
    # PKTABLE_NAME:  Name of the table containing the primary key.
    # PKCOLUMN_NAME: Name of the column containing the primary key.
    # FKTABLE_CAT:   Name of the catalog for the table containing the foreign key. The value is NULL if this table does not have catalogs.
    # FKTABLE_SCHEM: Name of the schema for the table containing the foreign key.
    # FKTABLE_NAME:  Name of the table containing the foreign key.
    # FKCOLUMN_NAME: Name of the column containing the foreign key.
    # KEY_SEQ:       1-indexed position of the column in the key.
    # UPDATE_RULE:   Integer value representing the action applied to the foreign key when the SQL operation is UPDATE.
    # DELETE_RULE:   Integer value representing the action applied to the foreign key when the SQL operation is DELETE.
    # FK_NAME:       The name of the foreign key.
    # PK_NAME:       The name of the primary key.
    # DEFERRABILITY: An integer value representing whether the foreign key deferrability is SQL_INITIALLY_DEFERRED, SQL_INITIALLY_IMMEDIATE, or SQL_NOT_DEFERRABLE.
    found = nil 
    stmt = IBM_DB::foreign_keys(@@conn, nil,  $config[:username], 'test_primary_keys')
    while row = IBM_DB::fetch_assoc(stmt)
      found = true 
      puts "#{idx}: #{row}"
      assert_match( $config[:username],row['pktable_schem'],"mismatch PKTABLE_SCHEM")
      assert_match('test_primary_keys',row['pktable_name'],"mismatch PKTABLE_NAME")
      assert_match('ID',row['pkcolumn_name'],"mismatch PKCOLUMN_NAME")
      assert_match("Q_#{ $config[:username]}_TEST_00001_ID_00001",row['pk_name'],"mismatch PK_NAME")

      assert_match( $config[:username],row['fktable_schem'],"mismatch FKTABLE_SCHEM")
      assert_match('TEST_FOREIGN_KEYS',row['fktable_name'],"mismatch FKTABLE_NAME")
      assert_match('ID',row['fkcolumn_name'],"mismatch FKCOLUMN_NAME")
      assert_match("Q_#{ $config[:username]}_TEST_00002_IDF_00001",row['fk_name'],"mismatch PK_NAME")

      assert_equal(1,row['key_seq'],"mismatch KEY_SEQ")
      assert_equal(3,row['update_rule'],"mismatch UPDATE_RULE")
      assert_equal(3,row['delete_rule'],"mismatch DELETE_RULE")
      assert_equal(7,row['deferrability'],"mismatch DELETE_RULE")
    end
    assert_not_nil(found,"mismatch no records found #{ $config[:username]}")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

