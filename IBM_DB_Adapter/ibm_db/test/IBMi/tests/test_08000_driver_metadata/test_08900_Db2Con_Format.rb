# File: test_08900_Db2Con_Format.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08900_Db2Con_Format < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  # -------------------------------
  # SYSIBM.DB2CON_FORMAT( 
  #   INOUT DATEFMT VARCHAR( 10 ), 
  #   INOUT DATESEP VARCHAR( 10 ), 
  #   INOUT TIMEFMT VARCHAR( 10 ), 
  #   INOUT TIMESEP VARCHAR( 10 ), 
  #   INOUT DECIMALSEP VARCHAR( 10 ))
  # The parameters in the procedure mentioned above can have values as shown below: 
  #   DATEFMT     - ISO, JIS, EUR, USA, MDY, DMY, YMD, JUL, JOB
  #   DATESEP     - SLASH, DASH, PERIOD, COMMA, BLANK, JOB 
  #   TIMEFMT     - ISO, JIS, EUR, USA, HMS 
  #   TIMESEP     - COLON, PERIOD, COMMA, BLANK, JOB
  #   DECIMALSEP  - PERIOD, COMMA, JOB
  # Example:
  #   CALL SYSIBM.DB2CON_FORMAT(NULL, NULL, 'HMS', 'BLANK', 'COMMA');
  # -------------------------------

  def test_0001_DB2CON_FORMAT
    stmt = IBM_DB::prepare(@@conn,'CALL SYSIBM.DB2CON_FORMAT(?, ?, ?, ?, ?)')
    assert_not_same( stmt, false, "prepare failed SYSIBM.DB2CON_FORMAT")

    datefmt = 'USA'
    datesep = 'SLASH'
    timefmt = 'USA'
    timesep = 'COLON'
    decsep = 'PERIOD'
    ret = IBM_DB::bind_param(stmt, 1, "datefmt", IBM_DB::SQL_PARAM_INPUT)
    assert_not_same( ret, false, "bind 1 failed SYSIBM.DB2CON_FORMAT")
    ret = IBM_DB::bind_param(stmt, 2, "datesep", IBM_DB::SQL_PARAM_INPUT)
    assert_not_same( ret, false, "bind 2 failed SYSIBM.DB2CON_FORMAT")
    ret = IBM_DB::bind_param(stmt, 3, "timefmt", IBM_DB::SQL_PARAM_INPUT)
    assert_not_same( ret, false, "bind 3 failed SYSIBM.DB2CON_FORMAT")
    ret = IBM_DB::bind_param(stmt, 4, "timesep", IBM_DB::SQL_PARAM_INPUT)
    assert_not_same( ret, false, "bind 4 failed SYSIBM.DB2CON_FORMAT")
    ret = IBM_DB::bind_param(stmt, 5, "decsep", IBM_DB::SQL_PARAM_INPUT)
    assert_not_same( ret, false, "bind 5 failed SYSIBM.DB2CON_FORMAT")

    ret = IBM_DB::execute(stmt)
    assert_not_same( ret, false, "execute failed SYSIBM.DB2CON_FORMAT")
    
    puts "Script ran clean"
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

