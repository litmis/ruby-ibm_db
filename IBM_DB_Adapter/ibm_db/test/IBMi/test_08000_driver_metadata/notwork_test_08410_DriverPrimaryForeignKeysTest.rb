# File: test_08410_DriverPrimaryForeignKeysTest.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08410_DriverPrimaryForeignKeysTest < Test::Unit::TestCase
  @@auth = Auth.new
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
    @@conn = @@auth.connectPool
  end

  def test_0003_meta_primary_keys
    idx = 0
    # TABLE_CAT:   Name of the catalog for the table containing the primary key. The value is NULL if this table does not have catalogs.
    # TABLE_SCHEM: Name of the schema for the table containing the primary key.
    # TABLE_NAME:  Name of the table containing the primary key.
    # COLUMN_NAME: Name of the column containing the primary key.
    # KEY_SEQ:     1-indexed position of the column in the key.
    # PK_NAME:     The name of the primary key.
    found = nil 
    stmt = IBM_DB::primary_keys(@@conn, nil, $curlib, 'TEST_PRIMARY_KEYS')
    while row = IBM_DB::fetch_assoc(stmt)
      found = true 
      @@auth.verbose "#{idx}: #{row}"
      # assert_match($db,row['TABLE_CAT'],"mismatch TABLE_CAT")
      assert_match($curlib,row['TABLE_SCHEM'],"mismatch TABLE_SCHEM")
      assert_match('TEST_PRIMARY_KEYS',row['TABLE_NAME'],"mismatch TABLE_NAME")
      assert_match('ID',row['COLUMN_NAME'],"mismatch COLUMN_NAME")
      assert_equal(1,row['KEY_SEQ'],"mismatch KEY_SEQ")
      assert_match("Q_#{$curlib}_TEST_00001_ID_00001",row['PK_NAME'],"mismatch PK_NAME")
    end
    assert_not_nil(found,"mismatch no records found #{$curlib}")
  end

  def test_0004_meta_foreign_keys
    idx = 0
    # PKTABLE_CAT:   Name of the catalog for the table containing the primary key. The value is NULL if this table does not have catalogs.
    # PKTABLE_SCHEM: Name of the schema for the table containing the primary key.
    # PKTABLE_NAME:  Name of the table containing the primary key.
    # PKCOLUMN_NAME: Name of the column containing the primary key.
    # FKTABLE_CAT:   Name of the catalog for the table containing the foreign key. The value is NULL if this table does not have catalogs.
    # FKTABLE_SCHEM: Name of the schema for the table containing the foreign key.
    # FKTABLE_NAME:  Name of the table containing the foreign key.
    # FKCOLUMN_NAME: Name of the column containing the foreign key.
    # KEY_SEQ:       1-indexed position of the column in the key.
    # UPDATE_RULE:   Integer value representing the action applied to the foreign key when the SQL operation is UPDATE.
    # DELETE_RULE:   Integer value representing the action applied to the foreign key when the SQL operation is DELETE.
    # FK_NAME:       The name of the foreign key.
    # PK_NAME:       The name of the primary key.
    # DEFERRABILITY: An integer value representing whether the foreign key deferrability is SQL_INITIALLY_DEFERRED, SQL_INITIALLY_IMMEDIATE, or SQL_NOT_DEFERRABLE.
    found = nil 
    stmt = IBM_DB::foreign_keys(@@conn, nil, $curlib, 'TEST_PRIMARY_KEYS')
    while row = IBM_DB::fetch_assoc(stmt)
      found = true 
      @@auth.verbose "#{idx}: #{row}"
      # assert_match($db,row['PKTABLE_CAT'],"mismatch PKTABLE_CAT")
      assert_match($curlib,row['PKTABLE_SCHEM'],"mismatch PKTABLE_SCHEM")
      assert_match('TEST_PRIMARY_KEYS',row['PKTABLE_NAME'],"mismatch PKTABLE_NAME")
      assert_match('ID',row['PKCOLUMN_NAME'],"mismatch PKCOLUMN_NAME")
      assert_match("Q_#{$curlib}_TEST_00001_ID_00001",row['PK_NAME'],"mismatch PK_NAME")

      # assert_match($db,row['FKTABLE_CAT'],"mismatch FKTABLE_CAT")
      assert_match($curlib,row['FKTABLE_SCHEM'],"mismatch FKTABLE_SCHEM")
      assert_match('TEST_FOREIGN_KEYS',row['FKTABLE_NAME'],"mismatch FKTABLE_NAME")
      assert_match('ID',row['FKCOLUMN_NAME'],"mismatch FKCOLUMN_NAME")
      assert_match("Q_#{$curlib}_TEST_00002_IDF_00001",row['FK_NAME'],"mismatch PK_NAME")

      assert_equal(1,row['KEY_SEQ'],"mismatch KEY_SEQ")
      assert_equal(3,row['UPDATE_RULE'],"mismatch UPDATE_RULE")
      assert_equal(3,row['DELETE_RULE'],"mismatch DELETE_RULE")
      assert_equal(7,row['DEFERRABILITY'],"mismatch DELETE_RULE")
    end
    assert_not_nil(found,"mismatch no records found #{$curlib}")
  end

  def test_9999_close
    @@auth.closePool
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

