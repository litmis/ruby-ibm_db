# File: test_08010_DriverColumnAnimal.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08010_DriverColumnAnimal < Test::Unit::TestCase
  @@conn = nil
  @@col_order_ord = ['ID', 'BREED', 'NAME', 'WEIGHT']
  @@col_order_meta = ['BREED', 'ID', 'NAME', 'WEIGHT']
  @@prv_order_meta = ['INSERT', 'REFERENCES', 'SELECT', 'UPDATE']

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_meta_columns
    idx = 0
    stmt = IBM_DB::columns(@@conn, nil, $config[:username], 'ANIMALS','%')
    while row = IBM_DB::fetch_assoc(stmt)
      puts "#{idx}: #{row}"
      # TABLE_CAT:          Name of the catalog. The value is NULL if this table does not have catalogs.
      # TABLE_SCHEM:        Name of the schema.
      # TABLE_NAME:         Name of the table or view.
      # COLUMN_NAME:        Name of the column.
      # DATA_TYPE:          The SQL data type for the column represented as an integer value.
      # TYPE_NAME:          A string representing the data type for the column.
      # COLUMN_SIZE:        An integer value representing the size of the column.
      # BUFFER_LENGTH:      Maximum number of bytes necessary to store data from this column.
      # DECIMAL_DIGITS:     The scale of the column, or NULL where scale is not applicable.
      # NUM_PREC_RADIX:     An integer value of either 10 (representing an exact numeric data type), 2 (representing an approximate numeric data type), or NULL (representing a data type for which radix is not applicable).
      # NULLABLE:           An integer value representing whether the column is nullable or not.
      # REMARKS:            Description of the column.
      # COLUMN_DEF:         Default value for the column.
      # SQL_DATA_TYPE:      An integer value representing the size of the column.
      # SQL_DATETIME_SUB:	Returns an integer value representing a datetime subtype code, or NULL for SQL data types to which this does not apply.
      # CHAR_OCTET_LENGTH:  Maximum length in octets for a character data type column, which matches COLUMN_SIZE for single-byte character set data, or NULL for non-character data types.
      # ORDINAL_POSITION:   The 1-indexed position of the column in the table.
      # IS_NULLABLE:        A string value where YES means that the column is nullable and NO means that the column is not nullable.
      assert_match($config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
      assert_match('ANIMALS',row['table_name'],"mismatch TABLE_NAME")
      assert_match(@@col_order_ord[idx],row['column_name'],"mismatch COLUMN_NAME")
      assert_equal(idx + 1,row['ordinal_position'],"mismatch ORDINAL_POSITION")
      assert_match('YES',row['is_nullable'],"mismatch IS_NULLABLE")
      assert_equal(1,row['nullable'],"mismatch NULLABLE")
      assert_not_nil(row['data_type'],"mismatch DATA_TYPE")
      assert_not_nil(row['type_name'],"mismatch TYPE_NAME")
      assert_not_nil(row['column_size'],"mismatch COLUMN_SIZE")
      assert_not_nil(row['buffer_length'],"mismatch BUFFER_LENGTH")
      assert_not_nil(row['sql_data_type'],"mismatch SQL_DATA_TYPE")
      idx += 1
    end
    count = @@col_order_ord.count
    assert_equal(count, idx, "missing row data")
  end

  def test_0002_meta_column_privileges
    idx = 0
    prv = 0
    stmt = IBM_DB::column_privileges(@@conn, nil, $config[:username], 'ANIMALS','%')
    while row = IBM_DB::fetch_assoc(stmt)
      puts "#{idx} #{row}"
      # TABLE_CAT:    Name of the catalog. The value is NULL if this table does not have catalogs.
      # TABLE_SCHEM:  Name of the schema.
      # TABLE_NAME:   Name of the table or view.
      # COLUMN_NAME:  Name of the column.
      # GRANTOR:      Authorization ID of the user who granted the privilege.
      # GRANTEE:      Authorization ID of the user to whom the privilege was granted.
      # PRIVILEGE:    The privilege for the column.
      # IS_GRANTABLE: Whether the GRANTEE is permitted to grant this privilege to other users. 
      assert_match($config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
      assert_match('ANIMALS',row['table_name'],"mismatch TABLE_NAME")
      assert_match(@@col_order_meta[idx],row['column_name'],"mismatch COLUMN_NAME")
      assert_nil(row['grantor'],"mismatch GRANTOR")
      assert_match($config[:username],row['grantee'],"mismatch GRANTEE")
      assert_match(@@prv_order_meta[prv],row['privilege'],"mismatch PRIVILEGE")
      assert_match('YES',row['is_grantable'],"mismatch IS_GRANTABLE")
      prv += 1
      if prv == @@prv_order_meta.count
        prv = 0
        idx += 1
      end
    end
    count = @@col_order_meta.count
    assert_equal(count, idx, "missing row data")
  end


  def test_0101_meta_num_fields
    stmt = IBM_DB::exec(@@conn,'SELECT * FROM animals ORDER BY breed')
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(4,nbr,"mismatch number fields select all")

    stmt = IBM_DB::exec(@@conn,"SELECT name, breed, 'TEST' FROM animals")
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(3,nbr,"mismatch number fields select 3")


    stmt = IBM_DB::exec(@@conn,'SELECT name, breed FROM animals where id > 200')
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(2,nbr,"mismatch number fields select 2")


    stmt = IBM_DB::exec(@@conn,"VALUES(1)")
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(1,nbr,"mismatch number fields values(1)")
  end

  def test_0102_meta_field_num
    stmt = IBM_DB::exec(@@conn,'SELECT * FROM animals ORDER BY breed')
    num1 = IBM_DB::field_num( stmt, "BREED" );
    num2 = IBM_DB::field_num( stmt, "NAME" );
    num3 = IBM_DB::field_num( stmt, "WEIGHT" );
    assert_equal(1,num1,"mismatch num2")
    assert_equal(2,num2,"mismatch num3")
    assert_equal(3,num3,"mismatch num4")
 end

 def test_0103_meta_field_num
    stmt = IBM_DB::exec(@@conn,'SELECT breed, COUNT(breed) AS number FROM animals GROUP BY breed ORDER BY breed')
    num2 = IBM_DB::field_num( stmt, "BREED" );
    num3 = IBM_DB::field_num( stmt, "NUMBER" );
    assert_equal(0,num2,"mismatch num2")
    assert_equal(1,num3,"mismatch num3")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
