# File: test_01170_DriverPrepareSales.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01170_DriverPrepareSales < Test::Unit::TestCase
  @@conn = nil

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE SALES')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE SALES (SALES_DATE DATE, SALES_PERSON VARCHAR(15), REGION VARCHAR(15), SALES INT)')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO SALES (SALES_DATE, SALES_PERSON, REGION, SALES) VALUES (?, ?, ?, ?)')
    $sales.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end

end
