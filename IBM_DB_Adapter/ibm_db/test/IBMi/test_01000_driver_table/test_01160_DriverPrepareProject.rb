# File: test_01160_DriverPrepareProject.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01160_DriverPrepareProject < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE PROJECT')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE PROJECT (PROJNO CHAR(6) NOT NULL, PROJNAME VARCHAR(24) NOT NULL, DEPTNO CHAR(3) NOT NULL, RESPEMP CHAR(6) NOT NULL, PRSTAFF DECIMAL(5,2), PRSTDATE DATE, PRENDATE DATE, MAJPROJ CHAR(6))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO PROJECT (PROJNO, PROJNAME, DEPTNO, RESPEMP, PRSTAFF, PRSTDATE, PRENDATE, MAJPROJ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)')
    $project.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end


  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
