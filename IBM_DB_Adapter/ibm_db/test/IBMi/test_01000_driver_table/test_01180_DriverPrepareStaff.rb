# File: test_01180_DriverPrepareStaff.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01180_DriverPrepareStaff < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0081_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE STAFF')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE STAFF (ID SMALLINT NOT NULL, NAME VARCHAR(9), DEPT SMALLINT, JOB CHAR(5), YEARS SMALLINT, SALARY DECIMAL(7,2), COMM DECIMAL(7,2))')
  end

  def test_0082_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO STAFF (ID, NAME, DEPT, JOB, YEARS, SALARY, COMM) VALUES (?, ?, ?, ?, ?, ?, ?)')
    $staffs.each { |a|
     puts "insert #{a[0]} #{a[1]} #{a[2]} #{a[3]}  #{a[4]}  #{a[5]}  #{a[6]} "
     ret = IBM_DB::execute(stmt,a)
    }
  end


  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

  
end
