# File: test_01410_DriverPreparePrimaryForeignKeysTest.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01410_DriverPreparePrimaryForeignKeysTest < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_primary_keys
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE test_primary_keys')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE test_primary_keys (id INTEGER NOT NULL, PRIMARY KEY(id))')
  end

  def test_0002_create_foreign_keys
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE test_foreign_keys')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE test_foreign_keys (idf INTEGER NOT NULL, FOREIGN KEY(idf) REFERENCES test_primary_keys(id))')
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
