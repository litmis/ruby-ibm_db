# File: auth_encrypt.rb
# encoding: utf-8                                                         
require './test_authorization/auth'

puts "Input: "
ARGV.each do|a|
  puts "  Argument: #{a}"
end

puts "Output: "
if (ARGV.count < 1)
  puts "Syntax:"
  puts "  ruby auth_encrypt.rb password [key]"
  puts "    password - user profile password to encrypt"
  puts "    [key]    - arbitrary pass phase for encrypt (32 characters required)"
  puts "             - missing uses default $pass_key (test_authorization/auth.rb)"
  exit(-1)
end

# user supplied password and key
if (ARGV.count > 1)
  $password = ARGV[0]
  $key = ARGV[1]
# default $pass_key test_authorization/auth/auth.rb
else
  $password = ARGV[0]
  $key = $pass_key
end
encrypt = Encrypt.new($key)
encoded = encrypt.encrypt($password)
puts "Encrypt #{$password} + #{$key} = #{encoded}"
decoded = encrypt.decrypt(encoded)
puts "Decrypt #{decoded}"


