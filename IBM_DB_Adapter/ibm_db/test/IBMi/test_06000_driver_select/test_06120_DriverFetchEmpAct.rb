# File: test_06120_DriverFetchEmpAct.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06120_DriverFetchEmpAct < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from EMP_ACT')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$employee_acts[idx][0]} #{$employee_acts[idx][1]} #{$employee_acts[idx][2]} #{$employee_acts[idx][3]} #{$employee_acts[idx][4]} #{$employee_acts[idx][5]}"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]} #{row[4]} #{row[5]}"
      assert_match( $employee_acts[idx][0], row[0], "EMPNO mismatch")
      assert_match( $employee_acts[idx][1], row[1], "PROJNO mismatch")
      assert_equal( $employee_acts[idx][2], row[2], "ACTNO mismatch nil")
      assert_equal( $employee_acts[idx][3], row[3], "EMPTIME mismatch")
      assert_match( $employee_acts[idx][4], row[4], "EMSTDATE mismatch")
      assert_match( $employee_acts[idx][5], row[5], "EMENDATE mismatch")
      idx += 1
    end
    count = $employee_acts.count
    assert_equal(count, idx, "missing row data")
  end

end

