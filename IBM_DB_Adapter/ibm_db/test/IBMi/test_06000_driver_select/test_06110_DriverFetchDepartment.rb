# File: test_06110_DriverFetchDepartment.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06110_DriverFetchDepartment < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from DEPARTMENT')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$departments[idx][0]} #{$departments[idx][1]} #{$departments[idx][2]} #{$departments[idx][3]} #{$departments[idx][4]}"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]} #{row[4]}"
      assert_match( $departments[idx][0], row[0], "DEPTNO mismatch")
      assert_match( $departments[idx][1], row[1], "DEPTNAME mismatch")
      if $departments[idx][2] != nil
        assert_match( $departments[idx][2], row[2], "MGRNO mismatch")
      else
        assert_equal( $departments[idx][2], row[2], "MGRNO mismatch nil")
      end
      assert_match( $departments[idx][3], row[3], "ADMRDEPT mismatch")
      if $departments[idx][4] != nil
        assert_match( $departments[idx][4], row[4], "LOCATION mismatch")
      else
        assert_equal( $departments[idx][4], row[4], "LOCATION mismatch nil")
      end
      idx += 1
    end
    count = $departments.count
    assert_equal(count, idx, "missing row data")
  end

end

